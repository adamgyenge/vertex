\documentclass{amsart}
\usepackage[margin=3.5cm]{geometry}

\title{2-categorical Heisenberg algebra}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{mathtools,bbold}
\usepackage{lmodern,microtype,csquotes}
\usepackage{tikz, tikz-cd}
\usetikzlibrary{decorations.markings}
\usepackage{comment, stackrel}
\usepackage{enumitem}
\usepackage{hyperref}

\theoremstyle{plain}
\newtheorem{Theorem}{Theorem}[section]
\newtheorem{Fact}[Theorem]{Fact}
\newtheorem{Claim}[Theorem]{Claim}
\newtheorem{Proposition}[Theorem]{Proposition}
\newtheorem{Corollary}[Theorem]{Corollary}
\newtheorem{Lemma}[Theorem]{Lemma}
\newtheorem{Conjecture}[Theorem]{Conjecture}

\theoremstyle{definition}
\newtheorem{Definition}[Theorem]{Definition}
\newtheorem{Assumption}[Theorem]{Assumption}
\newtheorem*{Notation}{Notation}

\theoremstyle{remark}
\newtheorem{Remark}[Theorem]{Remark}
\newtheorem{Remarks}[Theorem]{Remarks}
\newtheorem{Example}[Theorem]{Example}
\newtheorem{Examples}[Theorem]{Examples}
\newtheorem{Warning}[Theorem]{Warning}

\input{macros}

\newcommand\NN{{\mathbb{N}}}
\newcommand\ZZ{{\mathbb{Z}}}
\newcommand\CC{{\mathbb{C}}}

\newcommand\as[1]{\mathbb{A}^{#1}}
\newcommand\ps[1]{\mathbb{P}^{#1}}

\newcommand\Ext{\operatorname{Ext}}
\newcommand\1{\mathbf{1}}
\newcommand\tr{\operatorname{tr}}
\newcommand\DD{\mathbb{D}}
\newcommand\isoto{\xrightarrow{\sim}}

\newcommand\catDPerf[1]{\mathrm{D}^{\mathrm{perf}}(#1)}
\newcommand\sO{{\sheaf O}}
\newcommand\sheafHom{\underline{\Hom}}

\DeclareMathOperator\Sym{Sym}
\newcommand\SL{\mathrm{SL}}

\newcommand\heisen[1]{\mathcal{H}_{#1}}
\mathchardef\mhyphen="2D
\newcommand{\da}[1]{\bigg\downarrow\rlap{$\scriptstyle{#1}$}}
\newcommand{\ra}[1]{\kern-1.5ex\xrightarrow{\ \ #1\ \ }\phantom{}\kern-1.5ex}
\newcommand{\la}[1]{\kern-1.5ex\xleftarrow{\ \ #1\ \ }\phantom{}\kern-1.5ex}


\newcommand\adam{\color{red} \'Ad\'am:\ }
\newcommand\clemens{\color{green!80!black} Clemens:\ }
\newcommand\timothy{\color{blue} Timothy:\ }

\sloppy

\begin{document}
\section{DG categories}
\subsection{Preliminaries}

A dg category is a linear category ${\cat V}$ enriched in the monoidal category of complexes of $k$-vector spaces. This means that for any $A,B \in \mathrm{Ob}({\cat V})$ there is given a complex $\mathrm{Hom}_{{\cat V}}(A,B)$. We denote by $H^\cdot({\cat V})$ the $k$-linear category with the same objects as ${\cat V}$ and
\[ \mathrm{Hom}_{H^\cdot({\cat V})}(A,B)= \bigoplus_i H^i \mathrm{Hom}_{{\cat V}}(A,B). \]
$H^\cdot({\cat V})$ is called the graded homotopy category of ${\cat V}$. Restricting to the 0-th cohomology of the $\mathrm{Hom}$ complexes one gets the homotopy category $H^0({\cat V})$.

A dg functor between dg categories is a functor preserving the enrichment, that is, a functor inducing morphisms of the $\mathrm{Hom}$-complexes. A dg functor $F \;:\; {\cat V} \to {\cat W}$ is called a quasi-equivalence if $H^{\cdot}(F) \;:\; H^{\cdot}({\cat V}) \to H^{\cdot}({\cat W})$ is full and faithful and $H^{0}(F) \;:\; H^{0}({\cat V}) \to H^{0}({\cat W})$ is essentially surjective. Given dg categories ${\cat V}$ and ${\cat W}$ the set of covariant dg functors ${\cat V}\to {\cat W}$ form the objects of a dg category $\mathrm{Fun}_{dg}({\cat V},{\cat W})$.


 % We denote the dg category $\mathrm{Fun}_{dg}(\CV^{op},DG(k))$ by $\mathrm{mod}\mhyphen\CV$ and call it the category of dg $\CV$-modules. This is a dg category with shift and cone functors, which are inherited from $DG(k)$. The Yoneda-embedding realizes the original dg category $\CV$ as a full dg subcategory of $\mathrm{mod}\mhyphen\CV$.

For any dg category ${\cat V}$ a pretriangulated hull $\mathrm{Pre\mhyphen Tr}({\cat V})$ (respectively a perfect hull $\mathrm{Perf}({\cat V})$) can be associated with in a functorial way that contains cones, cones of cones,\dots (resp. direct summands thereof). For the details see \cite{bondal2004grothendieck}.
%The pretriangulated hull $\mathrm{Pre\mhyphen Tr}(\CV)$ of $\CV$ is the smallest full dg subcategory of $\mathrm{mod}\mhyphen\CV$ that contains $\CV$ and is closed under isomorphisms, direct sums, shifts, and cones. The perfect hull $\mathrm{Perf}(\CV)$ of $\CV$ is the full dg subcategory of $\mathrm{mod}\mhyphen\CV$ consisting of semi-free dg modules that are homotopy equivalent to a direct summand of an object in $\mathrm{Pre\mhyphen Tr}(\CV)$. %The homotopy category $H^0(\mathrm{Pre\mhyphen Tr}(\CV))$ is denoted as $\mathrm{Tr}(\CV)$.
A dg category ${\cat V}$ is called pretriangulated (resp. strongly pretriangulated), if the natural embedding ${\cat V} \to \mathrm{Pre\mhyphen Tr}({\cat V})$ is a quasi-equivalence (resp. dg equivalence). A dg category is called perfect, if ${\cat V}\to \mathrm{Perf}({\cat V})$ is a quasi-equivalence. If ${\cat V}$ is a pretriangulated category, then $H^{0}({\cat V})$ is naturally a triangulated category, and ${\cat V}$ is called an enhancement of $H^{0}({\cat V})$.
%If ${\cat V}$ is a pretriangulated category, then $H^{0}({\cat V})$ is naturally a triangulated category. Given a triangulated category ${\cat T}$, an enhancement of ${\cat V}$ is a pretriangulated category ${\cat V}$ with an equivalence of triangulated categories $\epsilon \colon H^{0}({\cat V}) \to \CT$. If $(\CV,\epsilon)$ is an enhancement of a triangulated category $\CT$ then any strictly full triangulated subcategory $\CS \subset \CT$ has an enhancement given by the full dg subcategory of $\CV$ of objects that go to objects of $\CS$ under $\epsilon$ together with the restriction of $\epsilon$ to the 0-th cohomology of this subcategory.

%\begin{Example}
%\label{ex:pretrcats}
%\begin{enumerate}
%\item $DG(k)=C(k\mhyphen \mathrm{mod})$ is the dg category of complexes of $k$-vector spaces, or dg $k$-modules.
%\item If $\CV$ is any dg category, then $\mathrm{Pre\mhyphen Tr}(\CV)$ is pretriangulated, and hence $\mathrm{Tr}(\CV)$ is triangulated.
%\item If $\mathcal{A}$ is any $k$-linear abelian category, then $C(\mathcal{A})$, the dg category of complexes over $\mathcal{A}$ is pretriangulated.
%\end{enumerate}
%\end{Example}



\begin{Example}
\label{ex:pretrcatsvar} 
\begin{enumerate} 
\item Let $X$ be a scheme of finite type over $k$. Let $\mathcal{O}_X\mhyphen \mathrm{mod}$ be the abelian category of all sheaves of $\mathcal{O}_X$-modules. Let $I(\mathcal{O}_X\mhyphen \mathrm{mod})$ be the full dg subcategory of $C(\mathcal{O}_X\mhyphen \mathrm{mod})$ consisting of h-injective complexes of injective objects. %, which are bounded below and have only finitely many cohomology sheaves, all quasi-coherent. 
Then $I(\mathcal{O}_X\mhyphen \mathrm{mod})$ is pretriangulated, and the composition $\epsilon_{I(\mathcal{O}_X\mhyphen \mathrm{mod})} \colon H^0(I(\mathcal{O}_X\mhyphen \mathrm{mod})) \to H^0(C(\mathcal{O}_X\mhyphen \mathrm{mod})) \to D(\mathcal{O}_X\mhyphen \mathrm{mod})$ is an equivalence \cite[2.1.1]{schnurer2015six} (see also \cite[Example 5.2]{bergh2016geometricity} and \cite[Thm. 14.3.1 (iii)]{kashiwara2005categories}). Therefore, $I(\mathcal{O}_X\mhyphen \mathrm{mod})$ is a dg enhancement of $D(\mathcal{O}_X\mhyphen \mathrm{mod})$.
\item It is known that the subcategory $D_{qc}(X) \subset D(\mathcal{O}_X\mhyphen \mathrm{mod})$ of complexes with quasi-coherent cohomology is thick. In fact, $D_{qc}(X)$ is known to be equivalent to $D(QCoh(X))$, where $QCoh(X)$ is the category of quasi-coherent $\mathcal{O}_X$-modules. Denote by $I_{qc}(X) \subset I(\mathcal{O}_X\mhyphen \mathrm{mod})$ the appropriate enhancement of $D_{qc}(X)$.
\item Similarly, the subcategory $D_{pf}(X) \subset D_{qc}(X)$ consisting of perfect complexes is thick. Its enhancement is $I_{pf}(X) \subset I_{qc}(X)$. When $X$ is smooth, then $D_{pf}(X)=D^b(Coh(X))$, where $Coh(X)$ is the category of coherent $\mathcal{O}_X$-modules.
\item The above examples can be generalized to stacks and derived categories associated with them. We will restrict our attention entirely to algebraic stacks of Deligne-Mumford type which are of finite type over $k$. One in fact obtains equivalent (resp. quasi-equivalent) triangulated (resp. dg) categories by considering sheaves on such stacks with respect to the smooth, \'etale, or fppf topologies, or any mix of these \cite[Claim 2.5]{arinkin2010perverse}.
\item In the rest of the paper we will use the perfect version of the derived categories and their dg enhancements both for schemes and for DM stacks. To ease the notation, we will drop the subscript and write only $D(X)=D_{pf}(X)$ and $I(X)=I_{pf}(X)$.
\end{enumerate}
\end{Example}
{\adam If we want to restrict to smooth projective varieties/stacks, then the above example can be shortened. However, if the Serre duality/Hom finites works for more general categories, we may leave this as is. }



We consider the $(\infty,2)$-category $\DGcatcont$ of $k$-linear (pre-triangulated?) dg-categories with continuous (i.e.~colimit preserving) functors \cite[Chapter~1]{AStudyInDAG1}.
This category has a symmetric monoidal product, which we will denote by $\otimes$.
Its unit is the category $\catVect$, i.e.~the dg enhanced derived category of $k$-vector spaces.
(In \cite[Section~1.10]{AStudyInDAG1} -- but not later in the book -- this monoidal product is denoted by $\otimes_{\catVect}$.)

{\adam If I understand correctly, you mean the tensor product ${\cat C} \otimes_{\catVect} {\cat D}$ from\cite[Chapter~1]{AStudyInDAG1}. This is not the same as ${\cat C} \otimes  {\cat D}$ that we use for the Fock space, and for which $\catVect$ is only a unit up to quasi-equivalence.}

{\clemens Are you sure that it isn't the same? I have not managed to trace through the definitions of \cite{AStudyInDAG1} and \emph{Higher Algebra}, but the product there has the correct properties as far as I can tell (it is compactly generated by $A \boxtimes B$ for $A \in \cat C^c$, $B \in \cat D^c$ and gives an isomorphism of (enhanced) derived categories $\catDbCoh{X} \otimes \catDbCoh{Y} \cong \catDbCoh{X \times Y}$). Also I think in the definitions of \cite{AStudyInDAG1} everything is up to quasi-equivalence because $\catVect$ is the derived category of vector spaces.}

{\adam If in\cite{AStudyInDAG1} everything is up to quasi-equivalence, then we are fine. However, localizing with respect to quasi-equivalences is not a completely trivial step. \cite{schnurer2018six} develops this, and he calls ENH the 2-category which is obtained from the the DGCAT by localizing at quasi-equivalences. I wrote this down a bit in Section \ref{subsec:dgenh} below. I think the continuity is not a big issue, we just have to localize $DGCAT_{cont}$ in the same way.}

{\clemens A note on terminology: It seems to me that in modern parlance an \enquote{equivalence} of dg categories is just a quasi-equivalence (see e.g.~\href{https://ncatlab.org/nlab/show/equivalence+of+dg-categories}{https://ncatlab.org/nlab/show/equivalence+of+dg-categories})}
{\adam Ok, I agree with this. }

\subsection{2-representations} Let $G$ denote a finite group. A 2-representation of $G$ on a linear category ${\cat T}$ consists of the following data \cite[Section 4]{ganter2008representation}:
\begin{enumerate}
	\item for each element $g \in G$, a linear functor $\rho(g) \colon {\cat T} \to {\cat T}$;
	\item for any pair of elements $(g,h)$ of a $G$ an isomorphism of functors;
	\[ \phi_{g,h}\colon (\rho(g) \circ \rho(h)) \xRightarrow{\cong}{} \rho(gh) \]
	\item and an isomorphism of functors
	\[  \phi_1 \colon \rho(1)  \xRightarrow{\cong}{} \mathrm{Id}_{\cat T};\]
\end{enumerate}
such that the following conditions hold:
\begin{enumerate}
	\item[(4)] for any $g,h,k \in G$ we have
	\[ \phi_{(gh,k)}(\phi_{g,h} \circ \rho(k))=\phi_{(g,hk)}(\rho(g) \circ \phi_{h,k}) ;\]
	\item[(5)] \[\phi_{1,g}=\phi_1 \circ \rho(g) \quad \textrm{and} \quad \phi_{g,1}=\rho(g) \circ \phi_1.\]
\end{enumerate}

\begin{Definition}
	Suppose we are given a 2-representation of $G$ on ${\cat T}$. A $G$-equivariant object of ${\cat T}$ is a pair $(A, (\epsilon_g)_{g \in G})$, where $A \in \mathrm{Ob}({\cat T})$ and $(\epsilon_g)_{g \in G}$ is a family isomorphisms
	\[ \epsilon_g\colon A \to \rho(g)A, \]
	satisfying the following compatibility conditions:
	\begin{enumerate}
		\item for $g=1$ we have
		\[\epsilon_1= \phi^{-1}_{1,A}\colon A \mapsto \rho(1)A;\]
		\item for any $g,h \in G$ the diagram
		\[
		\begin{array}{ccc}
		A & \ra{\phantom{AA}\epsilon_g \phantom{AA}} & \rho(g)(A) \\
		\da{\epsilon_{gh}} & & \da{\rho(g)(\epsilon_h)} \\
		\rho(gh)(A) & \la{\phi_{g,h,A}} & \rho(g)(\rho(h)(A))
		\end{array}
		\]
		is commutative.
	\end{enumerate}
	A morphism of equivariant objects from $(A, (\epsilon_g)_{g \in G})$ and $(B, (\eta_g)_{g \in G})$ is a morphism $f\colon A \to B$ compatible with the $G$-action. That is, a morphism for which all the following diagram is commutative:
	\[
	\begin{array}{ccc}
	A & \ra{\phantom{A}\epsilon_g \phantom{A}} & \rho(g)(A) \\
	\da{f} & & \da{\rho(g)(f)} \\
	B & \ra{\phantom{A}\eta_g \phantom{A}} & \rho(g)(B).
	\end{array}
	\]
	The category of $G$-equivariant objects in ${\cat T}$ is denoted as ${\cat T}^G$.
\end{Definition}

\begin{Example}
	\label{ex:equivtriang}
	For a triangulated category ${\cat T}$ the category ${\cat T}^G$ is additive and comes with a natural shift functor and a class of distinguished triangles. Namely, the shift functor on ${\cat T}^G$ is given by $(A, (\epsilon_g)_{g \in G}) \mapsto (A[1], (\epsilon_g[1])_{g \in G})$, and a triangle in ${\cat T}^G$ is distinguished if and only if the underlying triangle in ${\cat T}$ is distinguished. In general, ${\cat T}$ being triangulated is not sufficient to guarantee that this defines a triangulated structure on ${\cat T}^G$. In case this does define a triangulated structure, we will simply say that ${\cat T}^G$ is triangulated. Correspondingly, for a pretriangulated category ${\cat V}$ with a $G$-action the category ${\cat V}^G$ is not necessarily pretriangulated. But for a strongly pretriangulated category ${\cat V}$, the category ${\cat V}^G$ is always strongly pretriangulated \cite[Proposition 3.7]{sosna2012linearisations}, and this also implies that $H^0({\cat V}^G)$ is triangulated. (Note that the paper \cite{sosna2012linearisations} uses contravariant (right) 2-representations instead of our covariant (left) 2-representations, but there is a bijective correspondence between the two theories). This is the case for the category $I(M)$ associated to a scheme $M$ (see Example \ref{ex:quotcat} below and also \cite{elagin2014equivariant} for a more detailed discussion).
\end{Example}

\begin{Example}
	One can equip every linear category ${\cat V}$ with the trivial $G$-action. That is, all $\rho(g)$ and $\phi_{g,h}$, as well as $\phi_1$ are defined to be the identities. In this case a $G$-equivariant object in ${\cat V}$ is the same as a representation of $G$ in ${\cat V}$, i.e. an object $V \in  {\cat V}$ and a homomorphism $G \to \mathrm{Aut}_{{\cat V}}(V)$ \cite[Example 2.1.5.]{SymCat}.
\end{Example}


\begin{Example}
	\label{ex:quotcat}
	Let $M$ be a scheme of finite type over $k$ equipped with an action of $G$. Let $I(M)^G$ be the dg category of $G$-equivariant objects in $I(M)$.
	Since $I(M)$ is strongly pretriangulated, so is $I(M)^G$ by Example \ref{ex:equivtriang}. The composition $\epsilon_{I(M)^G} \colon H^0(I(M)^G) \to H^0(C(\mathcal{O}_M\mhyphen \mathrm{mod})^G) \to D(M)^G$ is an equivalence. By  \cite[Proposition 4.3]{sosna2012linearisations} there is a dg equivalence $I(M)^G \cong I([M/G])$, which induces a triangulated equivalence $D(M)^G=D([M/G])$ (see also \cite[Theorem 9.6]{elagin2011cohomological}).
\end{Example}
\subsection{Tensor products and symmetric powers}
The completed tensor product of two pretriangulated dg categories ${\cat V}$ and ${\cat W}$ is defined as
\[ {\cat V} \boxtimes {\cat W}= \mathrm{Perf}({\cat V}\boxtimes' {\cat W}), \] 
the perfect hull of the usual (uncompleted) categorical tensor product ${\cat V}^{\boxtimes' n}$ \cite{bondal2004grothendieck}.
\begin{Example}
\label{ex:geomprod}
By \cite[Theorem 5.5]{bondal2004grothendieck}, if $X_1,\dots,X_n$ are smooth projective schemes, then $I(X_1) \boxtimes \dots \boxtimes I(X_n)$ is quasi-equivalent to $I(X_1 \times \dots \times X_n)$. %A conjecture is that an analogous result holds for DM stacks (see Proposition \ref{prop:bgmult} below, and the remark before it).
\end{Example}


Let ${\cat V}$ be a pretriangulated dg category. The $n$-th (uncompleted) tensor power of ${\cat V}$ is ${\cat V}^{\boxtimes' n}:={\cat V} \boxtimes' \cdots \boxtimes' {\cat V}$ where there are $n$ factors in the tensor product. The $n$-th completed tensor power of ${\cat V}$ is ${\cat V}^{\boxtimes n}:={\cat V} \boxtimes \cdots \boxtimes {\cat V}=\mathrm{Perf}({\cat V}^{\boxtimes' n})$. For short, we will also denote these by ${\cat V}^{n'}={\cat V}^{\boxtimes' n}$ and ${\cat V}^n={\cat V}^{\boxtimes n}$ respectively. Let the symmetric group $S_n$ act on the objects generating ${\cat V}^{n'}$ and ${\cat V}^{n}$ by the transformation
\[ \sigma (A_1 \otimes \cdots \otimes  A_n)=A_{\sigma^{-1}(1)} \otimes \cdots \otimes A_{\sigma^{-1}(n)},\]
and similarly by component wise permutation on the $\mathrm{Hom}$ complexes. Taking the identity for all $\phi_{g,h}$'s we obtain a 2-representation of $S_n$ on ${\cat V}^{n}$. The uncompleted and completed $n$-th symmetric powers are defined as the category of $S_n$-equivariant objects in ${\cat V}^{n'}$ and ${\cat V}^{n}$ respectively \cite{SymCat}: 
\[\mathrm{Sym}^n({\cat V})':=({\cat V}^{n'})^{S_n} \quad \textrm{and} \quad\mathrm{Sym}^n({\cat V}):=({\cat V}^{n})^{S_n}.\]

\begin{Example}
	\label{ex:symcomp}
	If $X$ is a smooth projective variety, then $\mathrm{Sym}^n(I(X))$ is the dg category of $S_n$-equivariant h-injective complexes of injective $\mathcal{O}_{X^n}$-modules. By Example \ref{ex:quotcat}, after taking zeroth cohomology we get that $H^0(\mathrm{Sym}^n(I(X)))=D([X^n/S^n])=D(X^n)^{S_n}$ is the derived category of $S_n$-equivariant perfect complexes on $X^n$ \cite[Example 2.2.8.(a)]{SymCat}.
\end{Example}

\subsection{The category of dg enhancements}
\label{subsec:dgenh}
Let $\Enhcat$ be the 2-(multi)category of dg enhancements \cite{schnurer2018six}. Its objects are
additive pretriangulated dg categories. Its 1-morphisms coincide with the corresponding 1-morphisms in $\DGcat$, that is, they are dg  functors. Its 2-morphisms can be represented by zig-zags of 2-morphisms in $\DGcat$, i. e. by zig-zags of honest natural dg transformations, where the arrows pointing in the wrong direction are quasi-equivalences. Equivalently, $\Enhcat$ is the target of the additive localization (see \cite[Section 2.1.3.]{schnurer2018six}) of $\DGcat$ with respect to the set of quasi-equivalences.

Let $\Enhcatcont$ be the modification of $\Enhcat$ with only continuous (i.e.~colimit preserving) 1-morphisms \cite[Chapter~1]{AStudyInDAG1}.
Equivalently, $\Enhcatcont$ is the target of the localization of $\DGcatcont$ with respect to the set of quasi-equivalences. {\adam TODO: Check this.} The symmetric monoidal structure on $\DGcatcont$ descends to $\Enhcatcont$ {\adam TODO: Check this.}; by an abuse of notation we will denote this again by $\otimes$.  Let $[1]=DG(k)^f$ be the dg category of finite dimensional complexes of $k$-vector spaces. We denote the unique indecomposable object in $[1]$ by 1. For any pretriangulated dg category ${\cat V}$ the functor 
\[ 
\begin{array}{c c c}
{\cat V}  &\to &[1]\boxtimes {\cat V} \\
 C & \mapsto & 1 \otimes C \\
 \varphi & \mapsto & \mathrm{Id}_1 \otimes \varphi
\end{array}
\]
is a quasi-equivalence. Hence $[1]$ is a unit  for the symmetric monoidal structure on $\Enhcatcont$.

In our construction below, some of the quasi-equivalences are defined on the level of dg categories go in the wrong direction. By the above construction, they become well defined functors in $\Enhcatcont$. Taking homotopy categories defines a functor $H^0: \Enhcatcont \to \TRcat$. Our construction descends this way to the level of triangulated categories.

{\adam The machinery of \cite{schnurer2018six} develops internal tensors, homs, etc. and adjunctions between them. We do not really use this. Instead, we use the (external) tensor structure on $\Enhcatcont$. If the underlying category is $I(X)$ for $X$ a space, there is a relationship between the internal and external tensor products: 
\[ I(X) \boxtimes' I(X) \subset I(X) \boxtimes I(X) \xrightarrow{\sim} I(X \times X),\quad (A,B) \mapsto pr_1^{\ast} A \underline{\otimes} pr_1^{\ast} B. \]
This is needed to reproduce the formulas of \cite{krug2018symmetric}.
}




\section{subspaces of 2-morphisms}

{\adam 
For every subspace (subcomplex) $U\subset \Hom(A,B)$ the Serre pairing defines an orthogonal complement $U^{\perp}:=U^{\perp_S} \subset \Hom(B, SA)$. Moreover, there is an identification $\Hom(A,B)/U \cong (U^{\perp})^{\ast}$. This, in turn, gives a direct sum decomposition $\Hom(A, B)=U \oplus (U^{\perp})^{\ast}$.
}
{\clemens The direct sum is non-canonical.}

{\adam 
\begin{Remark}
It's worth to introduce some further notations.
If $U \subset \mathrm{Hom}(A,B)$ is a subspace (subcomplex), then 
\[
    \begin{tikzpicture}[baseline=0]
        \draw[->] (0,0) node[below] {$\PP_A$} -- node[label=right:{$U$},circle, fill=black, inner sep=0pt,minimum size=4pt, pos=0.5] {} (0,1) node[above] {$\PP_{B}$};
    \end{tikzpicture}, \quad
    \begin{tikzpicture}[baseline=0]
        \draw[->] (0,1) node[above] {$\QQ_A$} -- node[label=right:{$U$}, circle, fill=black, inner sep=0pt,minimum size=4pt, pos=0.5] {} (0,0) node[below] {$\QQ_{B}$};
    \end{tikzpicture}
\]
denotes the linear spaces of the appropriate arrows for all $\alpha \in U$. For a subcomplex $U \subset \mathrm{Hom}(A,B)$
\[
    \begin{tikzpicture}
                \draw[->] (0,0) node[above] {$\QQ_A$} arc[start angle=-180, end angle=0, radius=.5] node[label=below:{$U$},pos=0.5]{} node[above] {$\PP_B$};
            \end{tikzpicture}
    \quad \textrm{and}\quad
    \begin{tikzpicture}
            \draw[->] (0,0) node[below] {$\PP_A$} arc[start angle=180, end angle=0, radius=.5] node[label=above:{$U$},pos=0.5]{} node[below] {$\QQ_B$};
        \end{tikzpicture}
\]    
will denote
\[
\begin{tikzpicture}
        \draw[->] (0,1) node[above] {$\QQ_A$} -- 
                            node[label=left:{$U$}, circle, fill=black, inner sep=0pt,minimum size=4pt, pos=0.5] {}
                            (0,0.5) arc[start angle=-180, end angle=0, radius=.5] node[label=below:{$U \otimes 1$},pos=0.5]{} --
                            node[label=right:{$\id_B$}, circle, fill=black, inner sep=0pt,minimum size=4pt, pos=0.5] {}
                            (1,1) node[above] {$\PP_B$};
                        %\draw (2,1) ;
                                        \draw (2,0) node {$=$};
                                        \draw[->] (3,1) node[above] {$\QQ_A$} --
                                            node[label=left:{$\id_A$}, circle, fill=black, inner sep=0pt,minimum size=4pt, pos=0.5] {}
                                            (3,0.5) arc[start angle=-180, end angle=0, radius=.5] node[label=below:{$U \otimes 1$},pos=0.5]{}--
                                            node[label=right:{$U$}, circle, fill=black, inner sep=0pt,minimum size=4pt, pos=0.5] {}
                                            (4,1) node[above] {$\PP_B$};
    \end{tikzpicture}
    \quad \textrm{and}\quad
\begin{tikzpicture}
        \draw[->] (0,-0.5) node[below] {$\PP_A$} -- 
                            node[label=left:{$U$}, circle, fill=black, inner sep=0pt,minimum size=4pt, pos=0.5] {}
                            (0,0) arc[start angle=180, end angle=0, radius=.5] node[label=above:{$U^{\ast} \otimes 1$},pos=0.5]{} --
                            node[label=right:{$\id_B$}, circle, fill=black, inner sep=0pt,minimum size=4pt, pos=0.5] {}
                            (1,-0.5) node[below] {$\QQ_B$};
                        %\draw (2,0) ;
                                        \draw (2,0) node {$=$};
                                        \draw[->] (3,-0.5) node[below] {$\PP_A$} --
                                            node[label=left:{$\id_A$}, circle, fill=black, inner sep=0pt,minimum size=4pt, pos=0.5] {}
                                            (3,0) arc[start angle=180, end angle=0, radius=.5] node[label=above:{$U^{\ast} \otimes 1$},pos=0.5]{}--
                                            node[label=right:{$U$}, circle, fill=black, inner sep=0pt,minimum size=4pt, pos=0.5] {}
                                            (4,-0.5) node[below] {$\QQ_B$};
    \end{tikzpicture}
\]
Here $U \otimes 1$ means the vector space (complex) generated by copies of the empty string. Note that the cap has as target the dual complex. This is visible also in Example \ref{ex:PAQAadj} below. The dual complex is a quotient of $\mathrm{Hom}(B,A)$. The lift of such a homomorphism can be composed with $\mathrm{Hom}(A,-)$.

The same convention also applies to the other cups and caps.
\begin{comment}
\[\quad
    \begin{tikzpicture}
        \draw[->] (0,0) node[below] {$\PP_A$} arc[start angle=0, end angle=180, radius=.5] node[label=above:{$U\otimes 1$},pos=0.5]{} node[below] {$\QQ_{S^{-1}(A)}$};
    \end{tikzpicture},\quad
    \begin{tikzpicture}
        \draw[->] (0,0) node[above] {$\QQ_{S^{-1}(A)}$} arc[start angle=0, end angle=-180, radius=.5] node[label=below:{$U\otimes 1$},pos=0.5]{} node[above] {$\PP_{A}$};
    \end{tikzpicture},\quad
    \begin{tikzpicture}
        \draw[->] (0,0) node[above] {$\QQ_{A}$} arc[start angle=-180, end angle=0, radius=.5] node[label=below:{$1$},pos=0.5]{} node[above] {$\PP_{A}$};
    \end{tikzpicture},\quad
\]
\end{comment}
\end{Remark}


}

\section{Enhanced structure}

\begin{Proposition} If ${\cat V}$ is (strongly pre)triangulated, then so is $\heisen{\cat V}'$ in a 2-categorical sense.
\end{Proposition}
\begin{proof}

The shift functor on 1-morphisms is defined by shifting the letters in the string. For example, $\PP_A \QQ_B[1]=\PP_{A[1]} \QQ_{B[1]}$.

The cone of the upward (resp. downward) pointing strand induced by $\alpha \in \mathrm{Hom}(A,B)$ is $\PP_{\mathrm{Cone}(\alpha)}$ (resp. $\QQ_{\mathrm{Cone}(\alpha)}$). % TODO: pictures?
%\[
%    \mathrm{Cone}\Bigg(\begin{tikzpicture}[baseline=-0.25]
%        \draw[->] (0,-0.5)  -- node[label=right:{$\alpha$},circle, fill=black, inner sep=0pt,minimum size=4pt, pos=0.5] {} (0,0.5);
%    \end{tikzpicture}\Bigg)=\PP_{\mathrm{Cone}(\alpha)}, \quad
%    \begin{tikzpicture}[baseline=0]
%        \draw[->] (0,1) node[above] {$\QQ_A$} -- node[label=right:{$\alpha$}, circle, fill=black, inner sep=0pt,minimum size=4pt, pos=0.5] {} (0,0) node[below] {$\QQ_{A'}$};
%    \end{tikzpicture}.
%\]

More interesting are the cones of the other 2-morphisms.
We have the following natural exact triangles.
%\begin{tikzpicture}
%\draw (0,0) node{1} -- (1,0) node{$\QQ_A \PP_A$};
%\end{tikzpicture}
Recall that if $U \subset \mathrm{Hom}(A,A)$, then $U^{\perp} \subset \mathrm{Hom}(A,S(A))=\mathrm{Hom}(S^{-1}(A),A)$, and there is a canonical identification $\mathrm{Hom}(A,A)/U \cong U^{\perp}$.
\[
\begin{tikzcd}[column sep = large]
U \otimes 1 \arrow{r}{ \begin{tikzpicture}[scale=0.5]
\draw[->] (0,0) arc[start angle=-180, end angle=0, radius=.5] node[label=below:{U},pos=0.5]{} ;
\end{tikzpicture} } & \QQ_A \PP_A \arrow{r}{ \begin{tikzpicture}[scale=0.5] 
				\draw[<-] (0,0) -- (1,1);
				\draw[->] (1,0) -- (0,1); %\end{tikzpicture};
				\draw[-] (1.25,0.5) node{\oplus};
%\oplus \begin{tikzpicture}[scale=0.5]
\draw[->] (2.5,0) arc[start angle=0, end angle=180, radius=.5] node[label=above:{\phantom{.^{\perp}}U^{\perp}},pos=0.5]{} ;
\end{tikzpicture} } & \PP_A \QQ_A \oplus ((U^{\perp})^{\ast} \otimes 1) \arrow{r}{0} & (U \otimes 1)[1]
\end{tikzcd}\]
The existence of such an exact sequence follows from the direct sum decomposition
\[ \QQ_A\PP_A=\mathrm{Hom}(A,A)\oplus \PP_A\QQ_A=(U \oplus (\mathrm{Hom}(A,A)/U)) \oplus \PP_A\QQ_A \cong U \oplus (U^{\perp})^{\ast} \oplus \PP_A\QQ_A.\]
%Since the isomorphism $(U^{\perp})^{\ast}\cong U^{\perp}$ is not canonical, the cone object is only well determined upto an isomorphism. But this isomorphism is canonical in $U$ already at the derived level. 
Similar exact sequences exist for the other generating planar diagrams.
\[
\begin{tikzcd}[column sep = large]
\PP_A\QQ_A \oplus ( (U^{\perp})^{\ast} \otimes 1)   \arrow{r}{ \begin{tikzpicture}[scale=0.5] 
				\draw[->] (0,0) -- (1,1);
				\draw[<-] (1,0) -- (0,1); %\end{tikzpicture};
				\draw[-] (1.25,0.5) node{\oplus};
%\oplus \begin{tikzpicture}[scale=0.5]
\draw[<-] (2.5,1) arc[start angle=0, end angle=-180, radius=.5] node[label=below:{\phantom{.^{\perp}}(U^{\perp})^{\ast}},pos=0.5]{} ;
\end{tikzpicture}  } & \QQ_A\PP_A \arrow{r}{ \begin{tikzpicture}[scale=0.5]
\draw[<-] (0,0) arc[start angle=180, end angle=0, radius=.5] node[label=above:{U},pos=0.5]{} ;
\end{tikzpicture}} & U \otimes 1 \arrow{r}{0} & \PP_A\QQ_A \oplus ( (U^{\perp})^{\ast} \otimes 1)[1]
\end{tikzcd}\]

\[
\begin{tikzcd}[column sep = large]
\PP_B\QQ_A  \arrow{r}{ \begin{tikzpicture}[scale=0.5] 
				\draw[->] (0,0) -- (1,1);
				\draw[<-] (1,0) -- (0,1); %\end{tikzpicture};
\end{tikzpicture}  } & \QQ_A\PP_B \arrow{r}{ \begin{tikzpicture}[scale=0.5]
\draw[<-] (0,0) arc[start angle=180, end angle=0, radius=.5] node[label=above:{\mathrm{Hom}(A,B)},pos=0.5]{} ;
\end{tikzpicture}} & \mathrm{Hom}(A,B) \otimes 1 \arrow{r}{0} & \PP_B\QQ_A[1]
\end{tikzcd}\]

\[
\begin{tikzcd}[column sep = large]
\mathrm{Hom}(A,B) \otimes 1  \arrow{r}{ \begin{tikzpicture}[scale=0.5]
\draw[->] (0,0) arc[start angle=-180, end angle=0, radius=.5] node[label=below:{\mathrm{Hom}(A,B)},pos=0.5]{} ;
\end{tikzpicture}  } & \QQ_A\PP_B \arrow{r}{ \begin{tikzpicture}[scale=0.5] 
				\draw[->] (0,0) -- (1,1);
				\draw[<-] (1,0) -- (0,1); %\end{tikzpicture};
\end{tikzpicture}} & \PP_B\QQ_A  \arrow{r}{0} & (\mathrm{Hom}(A,B) \otimes 1 )[1]
\end{tikzcd}\]
The last two of these are actually special cases of the first two.
%{\adam Surprisingly, this seems to be a unique sequence. The dualities have to be thought through carefully.}
These sequences define the cones/cocones of all the generating planar diagrams. These cones are also compatible with the relations between the diagrams. If the cones are functorial ${\cat V}$, the cones in $\heisen{\cat V}'$ are also unique up to \emph{functorial} isomorphism, because the sequences above are always direct sums. %Hence, if ${\cat V}$ is pretriangulated, the  in it, hence in $\heisen{\cat V}'$ as well
\end{proof}


\bibliographystyle{amsplain}
\bibliography{2cat}
\end{document}

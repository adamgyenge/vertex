\documentclass{article}
\usepackage[margin=3.5cm, marginpar=3cm]{geometry}

%%%%%%%%%%%%%%%%%%
% Packages
%%%%%%%%%%%%%%%%%%

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amsfonts,amssymb,amsthm,stackrel,leftidx}
\usepackage{mathtools,bbold}
\usepackage{lmodern,microtype,csquotes}
% \usepackage{euflag}

\usepackage{tikz, tikz-cd}
\usetikzlibrary{decorations.markings}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{decorations.pathmorphing}
\usetikzlibrary{calc}

\usepackage{comment}

\usepackage{enumitem}
\setlist[enumerate,1]{label=(\arabic*)}

\usepackage[bookmarksdepth=2]{hyperref}

%%%%%%%%%%%%%%%%%%
% Theorem-likes
%%%%%%%%%%%%%%%%%%

\theoremstyle{plain}

% Unnumbered and alphabetically numbered environments for the intro
\newtheorem*{Theorem*}{Theorem}
\newtheorem*{Conjecture*}{Conjecture}
\newtheorem{IntroTheorem}{Theorem}
\renewcommand{\theIntroTheorem}{\Alph{IntroTheorem}}


\newtheorem{Theorem}{Theorem}[section]
\newtheorem{Fact}[Theorem]{Fact}
\newtheorem{Claim}[Theorem]{Claim}
\newtheorem{Proposition}[Theorem]{Proposition}
\newtheorem{Corollary}[Theorem]{Corollary}
\newtheorem{Lemma}[Theorem]{Lemma}
\newtheorem{Conjecture}[Theorem]{Conjecture}

\theoremstyle{definition}
\newtheorem{Definition}[Theorem]{Definition}
\newtheorem{Assumption}[Theorem]{Assumption}
\newtheorem*{Notation}{Notation}

\theoremstyle{remark}
\newtheorem{Remark}[Theorem]{Remark}
\newtheorem{Remarks}[Theorem]{Remarks}
\newtheorem{Example}[Theorem]{Example}
\newtheorem{Examples}[Theorem]{Examples}
\newtheorem{Warning}[Theorem]{Warning}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set up numbering system
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Equation numbered in sections
\numberwithin{equation}{section}
% Equations and theorem-likes numbered with the same counter
\makeatletter
\let\c@equation\c@Theorem
\makeatother

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Macros
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Citations to the stacks project
\newcommand{\stackcite}[1]{\cite[\href{http://stacks.math.columbia.edu/tag/#1}{Tag~#1}]{stacks-project}}

% All the math macros
\input{macros}

% Macros for adding notes
\newcommand\adam{\color{red} \'Ad\'am:\ }
\newcommand\clemens{\color{green!80!black} Clemens:\ }
\definecolor{purple}{rgb}{0.5, 0.0, 0.5}
\newcommand\timothy{\color{purple} Timothy:\ }
\newcommand\rev{\color{blue}\ }  % For the major changes in the revision
\usepackage{mparhack}

\title{Response to the report dated 18 October 2022}
\author{\'Ad\'am Gyenge, Clemens Koppensteiner and Timothy Logvinenko}
%\author{Clemens Koppensteiner}
%\author{Timothy Logvinenko}

\begin{document}
\maketitle

Dear Editors and Referees,

\vspace{0.8cm}

We thank our referees for the highly detailed and encouragingly 
constructive report on our manuscript. For an author it is a pleasure 
and a privilege to receive reports like this which help to
improve the paper so much. We can't be grateful enough. 

We are also thankful to the editors for their patience while we were
working on this revision. We wanted to be as thorough in dealing with 
the issues and the questions raised by the referees, as they were in
raising them. These questions, moreover, led us to think about
and eventually prove several new results. This accounts for most
of the delay. 

We hope we addressed all the issues raised by the referees. Most
importantly, they wrote:

\vspace{0.25cm}

{\em ``...the authors leave various intriguing aspects (a quantum version,
geometric and representation-theoretic applications, using other
additive invariants, ...) for future work. If (some of) these were
already included, we would recommend publication.''}

\vspace{0.25cm}

The referees are completely right about the issues left out. However
this paper, already near hundred pages long, was always intended to
be an opening gambit. It were to introduce these new constructions which 
we would then spend several following papers studying and applying to 
geometry and representation theory. We beg our referees to let us 
do these applications justice and not to make us cram them hurriedly into 
this paper which is already too long. 
 
However, to prove that we can answer the above demand of the referees, 
we chose two of the problems they mentioned -- a quantum version and
other additive invariants -- and did them for this paper.  For the other additive invariants, following
the excellent suggestion (Question 30) by the referees we chose zeroth
Hochschild homology. We added a major new Section 9 to the paper.
There we construct Hochschild categorification/decategorification, show 
that all our main results hold for it, and then turn one of our main
conjectures into a theorem by proving that our categorical Fock space
$\fcat\basecat$ is a categorification of the classical Fock space
$F^{H}_{\basecat}$ of $HH_0(\basecat)$.  
For the quantum version, we only wrote a very brief Subsection 6.5 in which 
we explained the basics and showed that our injective map of 
$\kk$-algebras $\pi\colon H_\basecat \rightarrow \numGgp{\hcat\basecat, \kk}$ 
extends to a map of $\kk[q,q^{-1}]$-algebras. 

Following another suggestion by the referees (Questions 19
and 31), we studied whether it is possible to reconstruct the original
category $\basecat$ from the Heisenberg $2$-category $\hcat\basecat$. 
Our progress is written up in the new Subsection 8.4. We came 
tantalisingly close to being able to completely reconstruct $\basecat$ 
(see \S8.4 and our responses to Questions 19 and 31), 
and trying to nail it was another major reason for the delay. 
In the end, we came close enough to be able to show that $\hcat\basecat$
of $\mathbb{P}^1$ and $\mathrm{pt} \sqcup \mathrm{pt}$ are distinct.
Reluctantly, we had to leave the rest to a future paper. 

We would also like to add that initially and even when we finished the
first submitted version of the paper, we thought that our numerical
Grothendieck group realisations will give a genuine categorification of the
Heisenberg algebra and the Fock space (by this we mean that $\pi$ and
$\phi$ are isomorphisms). Note that even in the simplest case 
of $X=\Spec(\kk)$ this statement was only conjectured by Khovanov in
his original paper. It was proved a decade later in 
the highly technical paper \cite{brundan2018degenerate}. On the other
hand, when writing this revision and thinking about the questions 
raised by the referees, we calculated some additional examples like 
the elliptic curve and found a counterexample. We wrote 
this counterexample up in the new Subsection 8.2.1.

We now tend to believe that our numerical Grothendieck group
decategorifications of $\hcat\basecat$ and $\fcat\basecat$ are, 
in most cases, strictly larger than the classical Heisenberg algebra 
$\halg\basecat$ and its Fock space $\falg\basecat$. 
However, our decategorifications always \emph{contain} 
$\halg\basecat$ and $\falg\basecat$, thus it becomes an interesting
new problem to compute the surplus and find some way to meaningfully
interpret it.  

Finally, in retrospective it is clear, given the nature of the Fock
space, that there is little hope it being the whole of 
the decategorification of its categorical counterpart unless 
the decategorifying invariant behaves nicely with respect to tensor
products and symmetric powers. If we wanted 
$\hcat\basecat$ and $\fcat\basecat$ to decategorify to 
$\halg\basecat$ and $\falg\basecat$ in full generality, we should
have worked with the Hochschild homology from the start. We have now
proved that $\fcat\basecat$ decategorifies to $\falg\basecat^H$
and we hope that in the Hochschild setting our conjecture
that $\hcat\basecat^H$ decategorifies to $\halg\basecat^H$
can also be turned into a theorem. It is the next thing on our
thinking menu. 

We carried out numerous changes in the text almost always in
accordance with the recommendations and suggestions. We are enclosing 
the revised manuscript with all the changes highlighted in blue. 

The following is a complete list of changes in accordance with the report.

Mathematical issues:

\begin{enumerate}
	\item On page 1, line -2 we explain that $\basecat$ can also 
be a graded or \dg category. In that case, we have to use the Euler
characterstic of $\Hom$ spaces. In the additive case, we understood
the Euler characteristic as the dimension of the $\homm$ space. That is
what it reduces to if one considers an additive category as a graded
or dg category concentrated in degree $0$. We added a clarification 
to the text.

	\item A new remark (Remark 2.14) was added after Corollary 2.13
following the referees' suggestion. 

	\item Corrected according to the referees' suggestion.

	\item Rewritten according to the referees' suggestion and moved to a new Section 2.2.1.

	\item Corrected according to the referees' suggestion.
	\item Corrected according to the referees' suggestion.
	\item Remark 3.28 was moved into Section 6 as Remark 6.7. A comment on the appearance of the $m=n=1$ case in the proof of Lemma 3.20 was added below Theorem 3.27.
	\item Our apologies, we have written this computation in a very 
abbreviated way, skipping intermediate steps such as the
computation of $P_b(a_1 \otimes \dots \otimes a_{\ho})$. As often
happens when you do this, it led to us making a mistake in the
computation. The referees are correct, and the formula for $\tau$ is
exactly what they suggested. We have rewritten this example with
all the missing steps reinstated and the computation corrected. 
Please note that $\sigma$ acting on $\basecat^{\otimes n}$ by acting 
on the indices via $\sigma^{-1}$ is the convention taken from 
Gantner-Kapranov, it ensures that we have the \em left \rm action 
of $\SymGrp{\ho}$ on $\basecat^{\otimes n}$.  
	\item A reference to Ganter-Kapranov was added here according
to the referees' suggestion. 
	\item We meant that to our knowledge it is still very
difficult to construct $(\infty, 2)$-categories by means of generators 
and relations. Most non-trivial examples are obtained via some sort
of universal property construction, because specifying all the higher
homotopical data by hand is usually unfeasible. We have changed 
the text of the paper to reflect that this is a difficulty 
perceived by the authors. If the referees were to point us at 
examples of non-trivial $(\infty, 2)$-categories being constructed 
via generators and relations, we would be grateful. We live to learn. 

        \item We do not know particular results in this direction. Our
motivation for this sentence was that the Serre functor (of the
principal block of) the category $\mathcal{O}$ has a very nice
geometric description \cite[Proposition 2.5]{TiltingExercises}. Thus
it stands to reason that our calculus could have a nice concrete
description that in turn can be used to a better understanding 
of some properties of category $\mathcal{O}$. A similar and closely
related phenomenon occurs in the paper \cite{Koppensteiner:Traces} of
one of the authors, which we added a refererence to in the paper.
	\item Corrected according to the referees' suggestion.  
	\item Corrected according to the referees' suggestion.  
	\item  In the new terminology this would be ``homotopy
unital'' and we corrected it accordingly. The referees made
a good point which we agree with. In 2-categorical structures
``strict'' means that the coherence morphisms are identity maps, 
``strong'' that they are isomorphisms, ``lax'' that they are just 
some morphisms. It makes sense that ``homotopy strong'' should 
mean that they are homotopy equivalences. Where possible, however, 
this could be abbreviated to just ``homotopy'', just like simply 
``monoidal'' usually means ``strong monoidal''. Thus ``strong homotopy
unital'' can just be abbreviated to ``homotopy unital''. 
	\item The proof clarified as requested by the referees.  
	\item We have added a remark on the classical correspondence
between representations of a group and modules over 
the associated skew group algebra. 
	\item We have added a brief explanation of the Ind and Res functors 
for $S_n$ to Section 3.5 just before what is now Example 3.31. 
	\item Yes, what we define to be $\sym^n\A$ agrees with
the definition of neither the (ordinary) $n$-th symmetric power
\cite[Defn.~2.2.1]{SymCat}
nor the completed $n$-th symmetrical power \cite[\S2.2.7]{SymCat}
in Gantner-Kapranov. The reason is the difference in paradigms
regarding DG-enhancements of triangulated categories. We work in
Morita enhancement setting, where a DG enhancement of a triangulated
category $T$ is a DG category $\A$ with $D_c(\A) \simeq T$.
They work in the older enhancement setting where it is a
DG category $\A$ with $H^0(\A)$. They make their definition to ensure 
that $H^0(\widehat{\sym}^n\A)$ is the correct symmetric product of $H^0(\A)$. 
We make our definition to ensure that $D_c(\sym^n\A) = H^0(\hperfA)$
is the correct symmetric product of $D_c(\A)$. Note that they work
with what they call perfect DG categories for which $D_c(\A) \simeq
H^0(\A)$. Hence, indeed, taking $\hperf$ of our definition of $\sym^n\A$
is equivalent to their $\widehat{\sym}^n\A$ --- the homotopy category
of both is the correct symmetric product of $H^0(\A)$!

Indeed, one of the points of our section \S4.8 was to rewrite the
definition of $\sym^n\A$ for the Morita setting, that is –– find 
the smallest natural category which Morita enhances the $n$-th symmetric
power of $D_c(\A)$. Gantner and Kapranov do something
that, from this point of view, is unnatural – they take regular $n$-th
tensor power of $\A$, then they take the perfect hull $\hperf$, and 
then they consider the group action and take the equivariant modules. 
We wanted a construction that works entirely on the level of the original 
category, without passing to modules halfway through, so that taking 
the perfect hull of that construction would give the category of
the equivariant modules. In other words, a categorification of 
the skew group algebra, whence our eventual definition of $\sym^n\A$. 

We have now clarified this point in the text of the paper. 

	\item Yes, we added Proposition 8.21 and Example 8.22 to show the non-equivalence of $\hcat{\catDGCoh{\ps 1}}$ and $\hcat{\catDGCoh {\mathrm{pt} \sqcup \mathrm{pt}}}$.
	\item The referees are correct, the differential satisfies 
the graded Leibniz rule for 1-composition. In fact, there is no
flexibility here – both graded Leibniz rules follow from the
definition of a DG bicategory, cf.~\S4.1. The rule for $2$-composition 
follow from the fact that $1$-morphism categories are DG categories
and the rule for $1$-composition follows from the fact that 
$1$-composition $C(b,c) \otimes C(a,b) \rightarrow C(a,c)$
is a DG functor and hence commutes with the differentials in the source 
and the target categories. 

We have now clarified this point in the text of the paper. 

	\item 

This is one of the main features of our \dg construction. As explained
in \S1.5, the main difficulty is that we only have a homotopy Serre
functor. We have therefore only a natural homotopy equivalence 
$\homm(b,Sa) \rightarrow \homm(a,b)^*$, but not its inverse, cf.  
\S4.7, Lemma 4.42. The definition of $\Psi$ in \S3.3 involves 
a map $\homm(a,b)^* \rightarrow \homm(b,Sa)$, thus we can't just
replicate it in DG setting. 

We follow a different approach, outlined in \S1.5 and performed in \S5.4. 
In the additive setup, as noted in \S3.3 after Remark 3.19, we have
two maps:
\begin{equation}
  \psi_1\colon \QQ_a\PP_b \to \Hom(a,b) \otimes_\kk \hunit
  \quad\text{and}\quad
  \psi_2\colon \Hom(a,b) \otimes_\kk \hunit \to \QQ_a\PP_b.
\end{equation}
The map $\Psi(\id)$ is the composition $\psi_2 \circ \psi_1$. The
relations $(3.11)$ and $(3.12)$ are then equivalent to the maps 
$$
  \QQ_a \PP_{b}
  \xrightarrow{
    \left[
      \begin{tikzpicture}[baseline={(0,0.15)}, scale=0.5]
        \draw[->] (1.5,0) -- (0.5,1);
        \draw[->] (1.5,1) -- (0.5,0);
      \end{tikzpicture}
      \,,\,
      \,\psi_1
    \right]
  }
 \PP_{b}\QQ_a \oplus \bigl(\Hom(a,b) \otimes_\kk \hunit \bigr) 
$$
$$  \PP_{b}\QQ_a \oplus \bigl(\Hom(a,b) \otimes_\kk \hunit \bigr) 
  \xrightarrow{
    \left[
      \begin{tikzpicture}[baseline={(0,0.15)}, scale=0.5]
        \draw[->] (0.5,0) -- (1.5,1);
        \draw[->] (0.5,1) -- (1.5,0);
      \end{tikzpicture}
      \,,\,
      \,\psi_2
    \right]
  }
  \QQ_a \PP_{b},
$$
being mutually inverse isomorphisms. Specifically, $(3.11)$ 
and the left relation in $(3.12)$ imply that 
$$ \PP_{b}\QQ_a \oplus \bigl(\Hom(a,b) \otimes_\kk \hunit \bigr) 
  \xrightarrow{
    \left[
      \begin{tikzpicture}[baseline={(0,0.15)}, scale=0.5]
        \draw[->] (0.5,0) -- (1.5,1);
        \draw[->] (0.5,1) -- (1.5,0);
      \end{tikzpicture}
      \,,\,
      \,\psi_2
    \right]
  }
  \QQ_a \PP_{b}
  \xrightarrow{
    \left[
      \begin{tikzpicture}[baseline={(0,0.15)}, scale=0.5]
        \draw[->] (1.5,0) -- (0.5,1);
        \draw[->] (1.5,1) -- (0.5,0);
      \end{tikzpicture}
      \,,\,
      \,\psi_1
    \right]
  }
 \PP_{b}\QQ_a \oplus \bigl(\Hom(a,b) \otimes_\kk \hunit \bigr) 
$$
is $\id_{\PP_{b}\QQ_a \oplus \bigl(\Hom(a,b) \otimes_\kk \hunit \bigr)}$, 
while the right relation in $(3.12)$ implies that the composition the other
way around is $\id_{\QQ_a \PP_{b}}$. 
Therefore, in presence of $(3.11)$ and the left relation of $(3.12)$
one can replace the right relation in $(3.12)$ by the condition that
$\left[
      \begin{tikzpicture}[baseline={(0,0.15)}, scale=0.5]
        \draw[->] (0.5,0) -- (1.5,1);
        \draw[->] (0.5,1) -- (1.5,0);
      \end{tikzpicture}
      \,,\,
      \,\psi_2
    \right]
$
is an isomorphism. 

In DG setting, we have the map $\psi_2$ but not the map $\psi_1$.
Therefore we impose the equivalents of $(3.11)$ and the left relation 
in $(3.12)$ as genuine relations in $\hcat*\basecat$ in $\S5.1$, and
then we force 
$\left[
      \begin{tikzpicture}[baseline={(0,0.15)}, scale=0.5]
        \draw[->] (0.5,0) -- (1.5,1);
        \draw[->] (0.5,1) -- (1.5,0);
      \end{tikzpicture}
      \,,\,
      \,\psi_2
    \right]
$
to be an isomorphism in the homotopy category by taking the Drinfeld
quotient by its cone in \S5.4 when constructing $\hcat\basecat$
out of $\hcat*\basecat$. This ensures that in the homotopy category 
of $\hcat\basecat$ the maps $\left[
      \begin{tikzpicture}[baseline={(0,0.15)}, scale=0.5]
        \draw[->] (1.5,0) -- (0.5,1);
        \draw[->] (1.5,1) -- (0.5,0);
      \end{tikzpicture}
      \,,\,
      \,\psi_1
    \right]$
and 
$\left[
      \begin{tikzpicture}[baseline={(0,0.15)}, scale=0.5]
        \draw[->] (0.5,0) -- (1.5,1);
        \draw[->] (0.5,1) -- (1.5,0);
      \end{tikzpicture}
      \,,\,
      \,\psi_2
    \right]
$ are mutually inverse isomorphisms, and hence the equivalent of 
the right relation in $(3.12)$ also holds as desired. 

We have now clarified this point in the text of the paper. 
 

	\item We thank the referees for drawing our attention to this
and apologise for how sloppily this was written up in the submitted
manuscript. The issue is more complicated than $\alpha$ and $e$
commuting up to homotopy. Indeed, in Section 6.2 they do not. 
However we changed our construction from $\alpha \mapsto \alpha
e$ to $\alpha \mapsto e{\alpha}e$. Then, in Section 6.2.1 we now show 
by idempotent absorption argument that 
$e{\alpha}e{\beta}e = e{\alpha}{\beta}e$ for the diagrams
that we use. Note that this is weaker then $\alpha$ or $\beta$ 
commuting with $e$. In Section 6.2.2 we now show via a more 
involved argument that $e{\alpha}e{\beta}e = e{\alpha}{\beta}e$
holds up to a desired numerical coefficient.

We have also rewritten Remarks 6.8 and 6.9, and added an explanation after
Remark 6.12 that the $\alpha \mapsto e{\alpha}e$ construction is not
automatically compatible with compositions unless $\alpha$ commutes
with $e$ and hence we check this compatibility by hand in Section 6.2. 

Finally, we've set $e := e_{\triv}$ as suggested by referees.

	\item We clarified the proof of Theorem 6.3(1) as suggested by
referees.
	\item We added a new Section 2.2.2 on the transposed
generators of the Heisenberg algebra as suggested by referees. 
We then referenced this new section in Section 6.3.
	\item We now deduce Proposition 6.23 (previously Proposition
6.18) from Theorem 6.3 and Lemma 6.22 (previously Lemma 6.19) 
as suggested by referees. 
	\item We clarified Remark 6.25 (previous Remark 6.21) as
suggested by referees.
	\item We thank the referees for drawing our attention to this. 
We gave a rather naive dimension bound to ensure that the ideal $I$
is generated by the classes $[\PP_a^\lambda]$ and $[\QQ_a^\lambda]$. 
We are not sure that the $n$-th graded part of the Fock space bound
would be right either: we are dealing with the ordinary 
Grothendieck groups $K_0$ and not numerical Grothendieck groups
$K_0^{num}$. For these we do not necessarily have the injective map 
from the Fock space of $K_0(\basecat,\,\kk)$ into 
$\bigoplus_{n} K_0(\symbc n,\,\kk)$. We decided to remove 
the mention of the dimension bound, and base the Remark on the 
assumption that $I$ is generated by the classes of 
$[\PP_a^\lambda]$ and $[\QQ_a^\lambda]$. 

To answer the second question, when $\basecat$ is the derived category
of a point $\text{Spec}\;\kk$, then ordinary Grothendieck groups and
numerical Grothendieck groups are the same and $K_0(\symbc n,\,\kk)$
is isomorphic to the $n$-th graded part of the Fock space. 
On the other hand, when $\basecat$ is the derived category of
an elliptic curve, the same argument that we now give for $K_0^{num}$ 
groups in \S8.2.1 also works for $K_0$ and 
shows that $K_0(\symbc 2,\,\kk)$ is non-isomorphic to 
the degree $2$ part of the corresponding Fock space. 

	\item We corrected Corollary 8.10 as suggested. 
 
	\item We corrected the proof of Corollary 8.10 as suggested. 

	\item We thank the referees for this brilliant suggestion.
As mentioned in the introduction to this response, we added a new
Section 9 on the Hochschild decategorification. You are completely
correct, the Hochschild cohomology is a lot better suited for this
categorification than the Grothendieck group. 

Indeed, the very formula for the dimension of the Fock space should
have suggested to us from the start that there is little hope of the
Fock space being the whole of the decategorification of its
categorical counterpart unless the decategorifying invariant satisfies
the K{\"u}nneth formula. And hence, by our Theorem 8.22, the same goes
for the Heisenberg algebra itself. We are very grateful to referees
for suggesting the Hochschild homology and thus steering us to the
setup where one of our main conjectures became a theorem.

\item Thank you for raising this question. The reconstruction of 
the original category from our Heisenberg category turned out to be 
a nontrivial problem. As explained in the newly added Section 8.4
we came tantalisingly close to being able to reconstruct the original
category fully, and a lot of the delay with the resubmission of the
revised manuscript was due to us trying to nail this. In the end, 
we realised that it will probably require
a separate paper. We wrote up our existing progress as an addition 
to Lemma 8.19 and as Lemma 8.24. It is enough to show that 
our Heisenberg categories of $\mathbb{P}^1$ and 
$\catDGCoh{\mathrm{pt} \sqcup \mathrm{pt}}$ are distinct. 
\end{enumerate}

Typos:

\begin{enumerate}
	\item The comparison results and examples are now referenced properly in Section 1.4 (previous Section 1.7), which was moved to just after the main results.
	\item Corrected
	\item Corrected
	\item Corrected according to the suggestion of the referee
	\item Sentence above Theorem A and paragraph after Theorem B were rewritten to clarify the statements of the paper. %A new Corollary C was added, so previous Theorem C is now Theorem D. 
	\item An explanation of the subscript $kc$ was included.
	\item Corrected as suggested.
	\item The Conjecture at the end of Section 1.3 is now labelled.
	\item Typo corrected.
	\item 'given by' was added to the sentence.
	\item Typo corrected.
	\item Typo corrected.  
	\item We've added an explanation for the order of the exposition.  
	\item First instance of 'ideal' was removed.
	\item Corrected throughout the text.
	\item Corrected.
	\item Corrected according to the suggestion of the referee. 
	\item Done.
	\item Done.
	\item Typo corrected.
	\item Corrected.
	\item Double negation was corrected. We added at the end that ``to the knowledge of the authors no treatment of Heisenberg algebras has been this general''.
	\item Reference to the paper \emph{Integral presentation of quantum lattice Heisenberg algebra} was added into the new section on quantum Heisenberg algebra.
	\item Text was added as suggested.
	\item Corrected.
	\item Corrected as suggested.
	\item Corrected.
	\item Corrected as suggested.
	\item Phrase removed.
	\item Remark 3.3 corrected.
	\item Chain of equalities simplified thanks to the referees.
	\item Third diagram was corrected by putting $\alpha$ to the left side.
	\item 'All functors' was replaced by 'The functors $\PP_a$ and $\QQ_a$' in Remark 3.35.
	\item We signposted the inhabitants of our m{\'e}nagerie with
brief introductions as suggested by the referees.  
	\item Font style changed to roman.
	\item Typo corrected.
	\item $\phi$ is replaced by $\phi \circ (\id \otimes 1_a)$ in both diagrams of Definition 4.6~(6).
	\item Remark 4.7 is now Definition 4.7.
	\item Section 4.2.1 now begins as ``A \emph{\dg (differential graded) category} $\A$ is...''.
	\item Compact and $\A$-perfect are emphasised at their first occurence.
	\item The word 'its' was removed' to avoid confusion.
	\item Subscript alignment corrected.
	\item This is intentional. We put the expression `tensor functor' into apostrophes to avoid confusion.
	\item DG enhancement emphasised.
	\item The reason we said `bounded' was that by definition 
the \dg categories in $\HoDGCatone$ and $\MoDGCatone$ are all small. 
Therefore we only get the enhancements of \em small \rm triangulated 
categories.  To make this clear, we changed the text in the
manuscript to ``Thus working in the Morita setting means working with
small Karoubi-complete triangulated categories, such as bounded derived
categories of abelian categories.''
	\item We explained meaning of the subscript `kc'. 
	\item We turned the passage that the referees pointed out into
the formal Definition 4.19 of a DG enhancement of a strict $2$-category.  
	\item We added the statement that this is a straightforward verification. 
	\item Typo corrected.
	\item The interchange law referenced as suggested by referees. 
	\item $\Dr$ put upright.
	\item $\old$ put upright.
	\item Typo corrected.
	\item Typo corrected.
	\item We rewrote Definition 4.45 (formerly Definition 4.44) to
fit better with other definitions of \dg categories in the paper.  
	\item $D_c$ was replaced by $D^b_{\mathrm{coh}}$.
	\item Corrected as suggested.
	\item Rephrased as suggested.
	\item Typo corrected.
	\item We added a brief explanation of the origin of the
 relations (5.12) and (5.13). 
	\item Corrected.
	\item Infinite dimensionality of $\homm_{\basecat}(a,b)$ 
is not an issue. What matters is that it is
a complex of vector spaces with finite-dimensional cohomology, which
holds since $\basecat$ is proper. Since every complex of vector spaces
is homotopy equivalent to the direct sum of its cohomologies, the
module $\homm_{\basecat}(a,b) \otimes_{\kk} \hunit$ is homotopy
equivalent to $\bigoplus_i H^i(\homm_{\basecat}(a,b))
\otimes_{\kk} \hunit$ which is a direct sum of a finite number of copies 
of $\hunit$ and hence a perfect module. Basically, in $\hperf$ we are
allowed to tensor with infinite dimensional complexes of vector spaces
as long as their cohomology is finite dimensional. 

We also think that our computation showing $d\psi_2 = 0$ was
correct, but we apologise for writing it out too briefly. Basically, 
we differentiate the map adjoint to $\psi_2$ which is a map of
complexes of vector spaces. Its differential is therefore given by the
formula
$$ d\psi_2^{\text{adj}}(\beta) =
d_{\text{target}}\bigl(\psi_2^{\text{adj}}(\beta)\bigr) -
\psi_2(d_{\text{source}} \beta) =
d\left(\smash[b]{\tikz[scale=0.5,baseline={(0,-0.25)}] \draw[->] (0,0) arc[start angle=-180, end angle=0, radius=.5] node[label=right:{$\beta$}, pos=0.7, dot] {};}\right)
-
\smash[b]{\tikz[scale=0.5,baseline={(0,-0.25)}] \draw[->] (0,0) arc[start angle=-180, end angle=0, radius=.5] node[label=right:{$d\beta$}, pos=0.7, dot] {};}
= 0. 
$$

We clarified both these points in the text of the paper. 

	\item Typos corrected.
	\item We made this statement more precise as follows: ``The 2-functor from  Corollary~\ref{cor:graded-to-graded} is an equivalence of graded 2-categories by the semisimplicity of $\basecat$. The category $\hcat\basecat$ is hence a \dg enhancement of Khovanov's categorification of the infinite Heisenberg algebra...''
	\item We clarified the statement as ``...gives a \dg enhancement...''
and added a reference was made to
\cite[Section~6]{cautis2012heisenberg}. The shifts of the form
$\langle 1 \rangle$ were a typo, we corrected them to $[1]$.
	\item Rewritten as suggested by referees.
	\item Thank you. We corrected this to ``graded homotopy category''. 
	\item It shouldn't be just $\alpha$, as then the assignment 
$\alpha \rightarrow \alpha e$ wouldn't commute with the
differentials. In particular, it would assing a non-closed map of
complexes to a closed $\alpha$!

Differentiating vertical morphism $\alpha$ in degree $0$ as a
map of twisted complexes yields vertical $d\alpha$ in degree $0$ but
also diagonal $\alpha \circ (1 - e)$ from degree $-1$ to degree $0$.
This diagonal component has to vanish for the assignment to commute
with differentials. For arbitrary $\alpha$ we do not have $\alpha =
\alpha e$ and hence this component doesn't vanish. On the other
hand, if the assigment sends $\alpha$ to $\alpha e$ or
$e \alpha e$ then the diagonal component always vanishes. 
 
 To understand what goes on, it helps to imagine a situtation where
the idempotent $e$ is split. Thus $\PP^n_a$ splits as a direct
sum $\PP^{(n)}_a \oplus \overline{\PP}^{(n)}_a$ and $e$ is a
projection to the summand $\PP^{(n)}_a$ followed by the inclusion, 
while $1 - e$ is the same for the other summand 
$\overline{\PP}^{(n)}_a$. Our twisted complex is homotopic 
to just $\PP^{(n)}_a$ in degree $0$ because all other summands in 
all other degrees are linked by identity differentials. However,  
differentiating vertical $\alpha$ in degree $0$ 
we get a vertical and a diagonal component and the latter 
is the sum of the two out of the four components of $\alpha$ -- 
the ones emerging from $\overline{\PP}^{(n)}_a$. We thus need
to precompose $\alpha$ with $e$ to kill these components. 

 In the revised manuscript, we switched over to $\alpha \rightarrow 
e \alpha e$ construction. In the toy example above, 
it corresponds to taking only the relevant $\PP^{(n)}_a \rightarrow 
\PP^{(n)}_a$ component of $\alpha$ and placing it vertically in degree $0$. 
This is clearly compatible with differentials, however not
automatically so with composition. In the toy example above, 
if $\alpha$ and $\beta$ have each all four components, then  
the relevant $\PP^{(n)}_a \rightarrow \PP^{(n)}_a$ component
of $\alpha \circ \beta$ would have two summands - the composition 
of the relevant components of $\alpha$ and $\beta$ and the
composition of the $\PP^{(n)}_a \rightarrow \overline{\PP}^{(n)}_a$
component of $\beta$ with the $\overline{\PP}^{(n)}_a \rightarrow \PP^{(n)}_a$ 
component of $\alpha$. Thus we still need to check by hand that 
the composition works, unless know that one of these components 
vanishes, i.e.  $e{\alpha}e = \alpha e$ or $e \beta e = e \beta$. 
 
\item Corrected
	\item We clarified the meaning of $\sum \alpha$ as requested by referees. 
	\item Corrected
	\item Removed the word ''nontrivial''. %{\adam This seems to refer to (6.10). Tim?}
	\item $\Hom^{\otimes n}_\basecat(a,b)$ was repaced with $\Hom_{\basecat^{\otimes n}}(a^n,b^n)$ in Remark 6.12
	\item Corrected
	\item We added examples for the diagram elements as suggested by referees. 
	\item Corrected
        \item We agree with the referees' suggestion to drop $\alpha_1
\vee \dots \vee \alpha_n$ in point (4). We thought about the
problem of using the symbol $\vee$ for pure tensors. Our concern is
that if there is no symbol between the terms, then the notation  
$\alpha_1 \alpha_2$ can mean both the morphism $\alpha_1 \vee \alpha_2$ 
in $\sym^2 \homm_\basecat(a,b)$ and the morphism $\alpha_1 \alpha_2$ in 
$\homm_{\basecat^{\otimes n}}(a^2, b^2)$. Since the latter is not
the image of former under $\psi$, we do not want to confuse the two. 
We kept the $\vee$ notation but introduced it properly in point (4).  
	\item Corrected.
	\item Typo corrected.
	\item Typo corrected.
	\item Sum now starts at $i=0$.
	\item We clarified our definition of the autoequivalence $F$. 
	\item Typo corrected.
	\item We added a paragraph about the case of $\QQ$'s to the
end of the proof of Lemma 6.22 (formerly Lemma 6.19).
	\item Yes, intertwine means $\iota^ne_\triv =e_\sign\iota^n$. We added this to the text and corrected the sentence. 
	\item Typo corrected.
	\item Sentences corrected.
	\item Reference was added to Section 2.2.1
	\item Typo corrected.
	\item We clarified this as ``A closed string diagram defines an endomorphism of $\hunit$. Some of these endomorphisms are non-trivial and are not subject to any relations. For example, those defined by clockwise bubbles, the
compositions of clockwise cups followed by clockwise caps. Thus 
the categories $\Hom_{\hcat\basecat}(\ho,\ho*)$ are not $\homm$-finite.''
	\item Corrected.
	\item Typo corrected.
        \item First we just make the statement for $\kk$-algebras.
Later in the new Section 6.5 we now show that it is in fact a map of
$\kk[q,q^{-1}]$-algebras.
	\item We added a reference to Section 8.2 for the definition of the codomain and the second map.
	\item We agree and we removed the explanation of what $\sym^n
\basecat$ is. 
	\item We replaced $\oplus$ with $\bigoplus$.
	\item Both objects are abelian groups. Hence, we replaced the term 'equivalent' with 'isomorphic'.
	\item We replaced ``has a representation'' by ``acts on''.
	\item If you are referring to the statement that 
$\rho = \phi(\sigma)\tau$ for some $\tau \in S_{n_1} \times \dots
\times S_{n_m}$ then this is not a group theoretic result,
but a statement about our functor $\Xi^{\PP}$. Indeed,
for any $\rho$ the $2$-morphism $\Xi^{\PP}(\rho)$ has only
unmarked strings. These must go from $\PP_a$ to $\PP_a$
for some indexing element $a \in \basecat$ which is the same
at the bottom and at the top of the string. Thus the assumption 
that $\Xi^{\PP}(\rho)$ is a morphism 
\[ \PP_{a_1}^{n_1} \PP_{a_2}^{n_2} \dots \PP_{a_m}^{n_m} 
\rightarrow \PP_{a_{\sigma(1)}}^{n_{\sigma(1)}} 
\PP_{a_{\sigma(2)}}^{n_{\sigma(2)}}
\dots \PP_{a_{\sigma(m)}}^{n_{\sigma(m)}} \]
implies that $\rho$ must send each $n_i$-tuple in 
the partition $(n_1, \dots, n_m)$ of $n$, in some order,
to the corresponding $n_i = n_{\sigma(\sigma^{-1}(1))}$-tuple 
in the permuted partition $(n_{\sigma(1)}, \dots, n_{\sigma(m)})$. 
In other words, doing $\rho$ is the same as individually permuting elements
of each $n_i$-tuple and then doing $\sigma$ to permute the
$n_i$-tuples. Thus $\rho = \phi(\sigma)\tau$. 

We now clarified this in the text of the paper.  

	\item We corrected this as suggested by referees.
	\item We added a reference to Lemma 7.3.
	\item We specified in the text that it is an isomorphism of 
$\basecat^{\otimes \ho}$-bimodules. 
	\item Typo corrected. 
	\item We added relevant references here (to Section 4.8) and
elsewhere throughout the text. 
        \item We replaced $=$ with $\simeq$ here. We presume that
the Hebrew letter in the remark of the referees was meant to be $\kk$.
Accordingly, we added a sentence to 1.11 that all of our tensor
products are over $\kk$ unless specifically noted otherwise. In other
words, blank $\otimes$ for modules and bimodules should be read as
being over $\kk$.  
	\item Typo corrected.
	\item Typo corrected.
	\item Typo corrected.
	\item We added references to the text.
	\item Typo corrected.  
	\item We removed the explanation of the perfect hull of a DG
bicategory after (2) in \S7.2, and replaced it by an appropriate
reference in $(2)$ itself. 
	\item Typo corrected.  
	\item We reformulated the statement of Lemma 7.10 using the
Yoneda embedding as suggested by referees. 
	\item Typo corrected. 
	\item We shifted the indexing to align with the earlier convention.
	\item Typo corrected. 
	\item See No. 105.
	\item We added a reference for the elementary 2-morphisms. 
	\item We added square brackets as suggested by referees.
	\item We replaced $=$s by $\simeq$s. 
	\item We clarified the Morita relation of the skew group
algebra to the coherent sheaves on quotient stack in Example 7.17. 
	\item Yes, we need to restrict to the sheaves supported 
at the (symmetric power) of the origin. We made this modification. 
	\item We replaced $a_i$ with $e_i$ everywhere in Section 7.4
	\item Typo corrected.  
	\item We added an explanation for the isomorphism in the middle. 
	\item Proof of Lemma 7.27 was added. 
	\item We clarified the case of general $\ho$ in the Proof of
Lemma 7.28. 
	\item We gave examples of $1$-morphisms of
$\modd\text{-}\basecat$ which $\bimodapx$ sends to bimodules which
aren't right-perfect. 
	\item We explained which $2$-morphisms of $\EnhCatKCdg$
are quasi-isomorphisms and why they are all homotopy equivalences. 
        \item In Definition 7.11 we defined the Fock space
$\fcat\basecat$ as a $1$-full subcategory of the $2$-category
$\bihperf(\EnhCatKCdg)$ which by Lemma 7.10 is a \dg enhancement of
the strict $2$-category $\EnhCatKC$ of enhanced triangulated
categories. This is the sense in which $1$-morphisms in the Fock space
are enhanced functors between enhanced triangulated categories.

 We clarified this in the text of the paper. 
 
	\item We changed ``sections'' to ``elements''.   
	\item We made the switching between the two viewpoints explicit. 	
	\item We corrected the product formula in Corollary 8.10 and elsewhere.
	\item We explained that this is a strong assumption and 
we added a countereample (that of an elliptic curve) in Section 8.2.1.
	\item We corrected the font type.
	\item We replaced $\oplus$ with $\bigoplus$.
	\item Typo corrected.
	\item Typo corrected.
	
	
	
	
	
	
	
	
	
	
\end{enumerate}

\bibliographystyle{amsplain}
\bibliography{references}

\end{document}

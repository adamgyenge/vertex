1. Some generalities: we are constructing a functor between bicategories enriched over $DGCat$ or, more generally, over $Ho(DGCat)$. Applying the functor
    $$H^0(-): Ho(DGCat) \to Cat$$ to the enrichment category produces bicategories enriched over $Cat$, i.e. ordinary bicategories. Thus when I say "on DG-level" I will mean our construction on the level of $Ho(DGCat)$-enriched bicategories, and when I say "on homotopy level" I will mean the construction obtained from ours on the level of ordinary bicategories.
 
2. On the homotopy level, what we are trying to do is quite simple.
    
    Our target category is well known - it is the strict 2-category $EnhCat_{kc}$ of Karoubi-complete enhanced triangulated categories. Its objects are small DG-categories, we think of each small DG-category $A$ as enhancing triangulated category $D_c(A)$. The category of 1-morphisms between $A$ and $B$ is the (skeleton of the) derived category $D_{B\text{-}perf}(A\text{-}B)$ of $B$-perfect $A$-$B$-bimodules. We think of each such bimodule $M$ as enhancing the exact functor $$(-) \overset{L}{\otimes} M\colon D_c(A) \to D_c(B).$$ Our source category is a strict 2-dg-category $H'_V$. On the homotopy level it produces a strict 2-category $H^0(H'_V)$. We then have an reasonably easy to construct strict 2-functor $$f\colon H^0(H'_V) \to EnhCat_{kc}.$$ It sends $P$ to the enhanced exact functor $\phi_a^*$, $Q$ to the enhanced exact functor $\phi_{a*}$, and $R$ to the enhanced exact functor $\phi_a^!$ which is isomorphic  to $\phi_{Sa}^*$ via Serre duality. The enhancements for $P$ and $Q$ are chosen to be the extension/restriction of scalars bimodules induced by $\phi_a$, and $R$ is enhanced as $\phi_{Sa}^*$. This functor is essentially the same functor as we had for the old version of $H'_V$ with just $P$s and $Q$s.

3. Our goal is therefore:

    1. To come up with a DG-enhancement of $EnhCat_{kc}$, that is with a $DGCat$ or $Ho(DGCat)$-enriched bicategory $MorDGCat$ whose $H^0$ is $EnhCat_{kc}$.
    1. To come up with "some kind of a 2-functor" $$F\colon H'_V \to MorDGCat$$ whose $H^0$ is the strict 2-functor $f\colon H^0(H'_V) \to EnhCat_{kc}$ discussed in (2).
  
4. I have had a ready candidate for $MorDGCat$, the DG-enhancement of $EnhCat_{kc}$. It is obtained from the bar category of bimodules, something I developed with Rina Anno over a course of several papers for precisely this purpose. A bar category of DG-bimodules is the DG-category _isomorphic_ to the category of DG-bimodules with $A_\infty$-morphisms between them. Thus it solves the problem of localising by quasi-isomorphisms. It is well-known that in the $A_\infty$ world any quasi–isomorphism is a homotopy equivalence. Moreover, the bar category has a simple intrinsic definition, which doesn’t bring in the complexities of the fully general $A_\infty$ machinery.

   Thus $MorDGCat$ could be defined with objects small DG-categories and 1-morphism categories between objects $A$ and $B$ being the full subcategories of $B$-perfect bimodules in $A\text{-}\overline{Mod}\text{-}B$, the bar-category of $A$-$B$-bimodules. The advantage of defining $MorDGCat$ via bar–categories is that it is then a $DGCat$-enriched bicategory, as opposed to only being $Ho(DGCat)$-enriched.
   
   However, I thought this definition of $MorDGCat$ would not work for us. The reason is that homotopy bimodules in $MorDGCat$ are almost never genuinely adjoint. The price for effectively resolving by bar complexes is that you introduce a lot of homologically trivial fluff, which prevents adjunctions from being genuine. In particular, the bimodules which we assign to our $P$s and $Q$s –– the restriction and extension of scalars bimodules –– are only homotopy adjoint in bar-categories.
   
   Since I thought that our goal was to find the world in which the images of our $P$s, $Q$s, and $R$s would be genuinely adjoint, I ruled out bar categories.
    
1. Fortunately, our monoidal Drinfeld quotient allows us an alternative definition of $MorDGCat$. In it, we define the morphism 1-categories between objects $A$ and $B$ in $MorDGCat$ to be the Drinfeld quotient of the DG-category of $A$-h-flat $B$-perfect $A$-$B$-bimodules by acyclics.

    This is only a $Ho(DGCat)$-enriched category, but here the bimodules we assign to $P$s and $Q$s –—  the bimodules which define $\phi^*_a$ and $\phi_{* a}$ —— are genuinely adjoint, so we are in good shape.
  
6. Unfortunately, when I was originally setting this all up, I was thinking in terms of $P$s and $Q$s only. There, it made no difference whether to work with functors $\phi^*_a$ and $\phi_{* a}$ or with the corresponding bimodules $_{\phi_a}(S^nV)$ and $(S^nV)_{\phi_a}$. As the functors $\phi^*_a$ and $\phi_{* a}$ are tensor functors, they are the functors of tensoring with these corresponding bimodules.
  
7. Let me make the statement in 6. precise. I will first need to talk about three different types of 2-functors out there, something that is very important for the final point of this e-mail. For this I need to quickly remind you of our definition of a 2-functor.
  
8. The full definition of a 2-functor is in Definition 4.6 of our paper, here is the abbreviated version:
    
   A 2-functor $F\colon C \to D$, where $C$ and $D$ are (enriched) bicategories, is the data of:
    
   1. A map $F\colon Ob(C) \to Ob(D)$.
   2. For any $a,b \in C$ a functor $F_{a,b}\colon C(a,b) \to D(Fa,Fb)$.
   3. "Composition coherator": Natural transformation $F_{comp}$ $$F_{b,c} \circ_{1\text{-comp } D} F_{a,b} \to F_{a,c}(-\circ_{1\text{-comp } C} -)$$ for all $a,b,c \in Ob(C)$.
   4. "Unitality coherator": Natural transformation $F_{unit}$ $$1_{Fa} \to F_{a,a}(1_a)$$ for all $a \in Ob(C)$.
    
   and this data has to satisfy some conditions.
  
1. A quick way to understand the coherators in 8c) and 8d) is that the coherator for  either operation is the natural transformation,
    
    > Operation in $D$ on $F$(operands) -> $F$(operation in $C$)
    
    Thus $F_{comp}$ is the natural transformation which given any two composable 1-morphisms in $C$ goes from
    
    > 1-composition in $D$ of their images under $F$
    
    to
    
    >  the image under $F$ of their 1-composition in $C$.
    
    And similarly, $F_{unit}$ goes
    
    > the 1-unit of $D(Fa,Fa)$  -> $F$(the 1-unit of $C(a,a)$)

1. The three types of 2-functors usually considered are:

   1. Strict 2-functors – these are the ones where $F_{comp}$ and $F_{unit}$ are identity maps. In other words, $F$ respects both composition and 1-units. When talking about strict 2-categories, one normally means this when saying "2-functor."
   2. Strong 2-functors - these are the ones where $F_{comp}$ and $F_{unit}$ are isomorphisms. In other words, $F$ respects both composition and 1-units up to isomorphisms.
   3. Lax 2-functors - everything else. $F_{comp}$ and $F_{unit}$ are anything. F doesn’t respect composition and 1-units, but we have a specified coherence morphism
       
       > $\text{operation } \circ F \to F \circ \text{operation}.$

1. In this language, we have a strong 2-functor $$DGBimod \to BigModCat$$ where:

   1. $DGBimod$ is the bicategory of small DG categories and their bimodules.
   1. $BigModCat$ is the strict 2-category whose objects are small DG-categories, and whose 1-morphism categories from $A$ to $B$ are DG-functors from $Mod(A)$ to $Mod(B)$.
    
   This 2-functor is identity on objects and sends a bimodule to the functor of tensoring by this bimodule. This functor is 2-fully faithful: it acts on the categories of 1-morphisms by fully faithful functors. Its essential image is 2-full subcategory of $BigModCat$ consisting of tensor DG-functors.
   
   In this sense, there really is no difference in working with bimodules or with tensor functors.
  
1. What we have naturally is a strict 2-functor $$G\colon H'_V \to BigModCat$$
  
   Things would have been easy if $G$ would have filtered through the inclusion of the (essential image of) subcategory $DGBimod_{B\text{-perf},\, A\text{-h-flat}}$ of the (tensor functors defined by) $B$-perfect and h-$A$-flat bimodules. This is because we defined $MorDGCat$ to be the Drinfeld quotient of $DGBimod_{B\text{-perf},\ \text{h-}A\text{-flat}}$ by acyclics. So we could have just composed a strong 2-functor $$G\colon H'_V \to DGBimod_{B\text{-perf}, \text{h-}A\text{-flat}}$$  with the natural functor from $DGBimod_{B\text{-perf},\, \text{h-}A\text{-flat}}$ into its Drinfeld quotient $MorDGCat$ and we are done constructing our $F$.
  
1. The images of $P$ and $Q$ under $G$ are indeed tensor functors given by h-$B$-perfect, h-$A$-perfect (and thus certainly h-$A$-flat) bimodules.

   Unfortunately, $G(R)$ is nothing of that kind.
   
   Originally we were working with $P$s and $Q$s, so in my head passing from functors to bimodules was automatic. Then we added $R$s. And only when sitting to write this all down I’ve realised that we have a problem:
  
   > $G(R)$ is not even a tensor functor.
  
1. Pretty much the only solution was to try and apply the bimodule approximation 2-functor. Indeed, we have a lax 2-functor $$Approx\colon BigModCat \to DGBimod$$ which is the identity on objects and sends any DG-functor $$F\colon Mod(A) \to Mod(B)$$ to its bimodule approximation. This is an $A$-$B$-bimodule $M$ which is essentially image of $F$ on representables. Indeed, pre–composing $F$ with $$Yoneda\colon A \to Mod(A)$$ gives $$F \circ Yoneda\colon A \to Mod(B)$$ and DG-functors from $A$ to $Mod(B)$ are the same as $A$-$B$-bimodules.

   A slicker way of saying the same thing is to define $$Approx(F) := F(A).$$ Let me unpack this. $F\colon Mod\text{-}A \to Mod\text{-}B$ defines a natural functor of right evaluation $$A\text{-}Mod\text{-}A \to A\text{-}Mod\text{-}B, \qquad N \mapsto F(N)$$ and we simply take the image of the diagonal bimodule under it.
  
1. In general, $$Approx: BigModCat \to DGBimod$$ is only a lax 2-functor. However, it is a strong 2-functor (indeed an strong 2-equivalence) on tensor functors.

   I was hoping that maybe we get lucky, and $Approx$ does send $G(R)$s to $B$-perfect, h-$A$-flat bimodules which would still be genuinely right adjoint to $G(Q)$s. Let us write $$M = Approx \circ G,$$ in other words $MP$, $MQ$, $MR$ are the bimodules we obtain out of $G(P)$, $G(Q)$, $G(R)$.
  
1. $MR$s are obviously $B$-perfect since they are quasi-isomorphic to $G(P)$s for Serre dual elements of $V$. They also magically turned out to h-$A$-flat.

   However, they most certainly turned out not to be genuinely adjoint to $MQ$s. They are, in fact, the right duals of $MQ$s. If $MR$ is an $A$-$B$-bimodule, then $MQ$ is a $B$-$A$-bimodule and we have $$MR = Hom_A(MQ, A).$$ These are well-known to be homotopy adjoint to $MQ$s, but this homotopy adjunction becomes genuine only in very special cases. And ours was not such a case.
  
1. The reason I thought it was important to ensure that $MP$s, $MQ$s and $MR$s are genuinely adjoint, is because strong/strict 2-functors have to send genuine adjunctions to genuine adjunctions. Indeed this is the very original problem we’ve set out to fix.

   So at this point, I was quite ready to despair. We had a strong 2-functor $$G\colon H'_V \to BigModCat,$$ but I had no way of sending that to MorDGCat without losing the genuine adjunctions, which would mean that we’d end up with a lax 2-functor.
  
1. I then asked myself, why is a lax 2-functor not good enough? I thought it was because we were constructing a 2-functor $$F\colon H'_V \to MorDGCat$$ which would produce a strict 2-functor $$f\colon H^0(H'_V) \to EnhCat_{kc}.$$ I didn’t consider lax functors, since an arbitrary lax functor on DG level doesn’t descend to a strong/strict functor on homotopy level.

1. This is where I had a realisation which in the retrospective is blindingly obvious: we would be quite happy with a homotopy-lax 2-functor! In other words, it would be enough to have a lax 2-functor whose coherators are not isomorphisms, but homotopy equivalences! Then, when we descend from DG-level to homotopy level, it would become a strong 2-functor as required.

1. Since $$G\colon H'_V \to BigModCat$$ is a strict 2-functor, we just need $$Approx\colon BigModCat \to MorDGCat$ to be homotopy 2-lax on $G(P)$, $G(Q)$, and $G(R)$s.

1. $Approx_{unit}$, the unitality coherator, is always the identity map. This is because the image of the identity functor under $Approx$ is the diagonal bimodule, which is 1-identity in $MorDGCat$. 
  
1. Now let’s look at $Approx_{comp}$, the composition coherator. It is defined as follows. Suppose you have two DG-functors $$U\colon Mod\text{-}A \to Mod\text{-}B \quad\text{and}\quad V\colon Mod\text{-}B \to Mod\text{-}C.$$ Then $$Approx(V \circ U) = VU(A),$$ while $$Approx(U) \otimes_B Approx(V) = U(A) \otimes_B V(B).$$ The coherator is given by a natural morphism $$U(A) \otimes_B V(B) \to VU(A)$$ which is the adjoint of the natural morphism $$U(A) \to Hom_C(V(B),\, VU(A) ),$$ given by the composition $$U(A) \simeq Hom_B(B, U(A)) \to Hom_C(V(B), VU(A)).$$
  
1. $G(P)$s and $G(Q)$s are tensor functors. $G(R)$s are functors $Hom_A(G(Q), - )$. Let us look at how the coherator $$U(A) \otimes_B V(B) \to VU(A)$$ defined above behaves on tensor and Hom-functors. Now miracles start to happen:
   1. If $V$ is a tensor functor $(-) \otimes_B N$, then the coherator is the natural isomorphism $$U(A) \otimes_B (B \otimes_B N) \xrightarrow{iso} U(A) \otimes_B N$$ induced by the natural isomorphism $$B \otimes_B (-) \simeq (-).$$
   2. If $V$ is a $Hom$ functor $Hom_B(N, -)$, then the definition above yields the coherator is the natural map $$Hom_B(N,B) \otimes U_A \to Hom_B(N,U(A))$$ given by identifying $U_A$ with $Hom_B(B, U_A)$ and then composition. This map is well known to be quasi-isomorphism when $N$ is $B$-h-perfect.

   Since $G(Q)$s are certainly left-h-perfect, the above means that on $G(P)$s, $G(Q)$s and $G(R)$s the coherator $Approx_{comp}$ is either an isomorphism or a quasi-isomorphism.

1. Define $$F\colon H'_V \to MorDGCat$$ to be the composition $$Approx \circ G.$$ G is a strict 2-functor, and we’ve shown in above that $Approx$ is homotopy-lax on the image of $G$.

1. We win. We have constructed a homotopy lax 2-functor $$F\colon H'_V \to MorDGCat.$$ It does send genuine adjunctions in $H'_V$ to only homotopy adjoint bimodules in $MorDGCat$. This is something we have been trying to avoid. But:
   1. On the homotopy level it induces a strong 2-functor $$H^0(F)\colon H^0(H'_V) \to H^0(MorDGCat)$$ which then becomes a strict 2-functor when we strictify $H^0(MorDGCat)$ to $EnhCat_{kc}$. Thus, as usual, we obtain something nice on homotopy level (strict 2-functor), while upstairs on DG-level it’s a bit of a mess (homotopy-lax 2-functor).
   1. What we really construct is a strict 2-functor $$G\colon H'_V \to BigModCat,$$ this is where all the representation theory happens. Everything there is nice and explicit. It only becomes homotopy-lax when we apply the well-known lax 2-functor $Approx$ of bimodule approximation.

1. It is worth seeing explicitly how a homotopy lax 2-functor only sends genuine adjunctions to homotopy ones. Let $$F\colon C \to D$$ be a homotopy lax 2-functor between two bicategories $C$ and $D$. Suppose $q$, $r$ are two adjoint 1-morphisms in $C$. We then have morphisms $$unit\colon id \to rq,\qquad counit\colon qr \to id.$$ The composition $$q \xrightarrow{q(unit)} qrq \xrightarrow{(counit)q} q$$ is then the identity 2-morphism. Then the composition $$F(q) \xrightarrow{F(q(unit))} F(qrq) \xrightarrow{F((counit)q)} F(q)$$ is also the identity 2-morphism.

   But because $F$ is lax, $F(qrq)$ is not isomorphic to $F(q)F(r)F(q)$! We only have a homotopy equivalence given by coherator morphism: $$F(q)F(r)F(q) \xrightarrow{coh} F(qrq).$$ Thus we do not automatically get $F(q)$ and $F(r)$ be adjoint in $D$. They are however homotopy adjoint. This is because in the homotopy category all the coherator morphisms become isomorphisms. Our conditions on coherator morphisms ensure that the composition $$F(q) \xrightarrow{F(q(unit))} F(qrq) \xrightarrow{F((counit)q)} F(q)$$ is equal to $$F(q) \xrightarrow{F(q)(new\ unit)} F(q)F(r)F(q) \xrightarrow{(new\ counit)F(q)} F(q)$$ where the new counit is defined as $$F(q)F(r) \xrightarrow{coh} F(qr) \xrightarrow{F(counit)} F(id_C) \xrightarrow{coh} id_D$$ and the new unit is defined as $$id_D \xrightarrow{coh^{-1}} F(id_C) \xrightarrow{F(unit)} F(rq) \xrightarrow{coh^{-1}} F(r)F(q).$$ Not that we can’t even define the new adjunction units unless coherator morphisms are invertible!
  
1. All of the above works just as well with $MorDGCat$ being defined via bar-categories. This is probably a better way of doing it, since then $MorDGCat$ would be a $DGCat$ enriched category and not $Ho(DGCat)$-enriched. Thus I do not need to make the perfect hull construction work on $Ho(DGCat)$-enriched categories.
  
1. The main point of the above is that we should have been trying, from the start, to construct a homotopy-lax 2-functor, and not a strong 2-functor. I have already asked myself whether we could have constructed a homotopy-lax 2-functor from $P$-and-$Q$-only version of $H'_V \to MorDGCat$. I am not sure we could, though. It seems to be difficult, as we’d have to have a very non trivial coherator which would somehow do all the Serre duality magic for us.

   I am convinced that the above is a better way of doing it – construct an honest, easy-to-work-with  strict 2-functor, and then make it into a homotopy-lax 2-functor by applying the bimodule approximation.

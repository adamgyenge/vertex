\documentclass[a4paper]{amsart}
\usepackage[margin=3.5cm]{geometry}

\title{The Heisenberg category of a category}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{mathtools,bbold}
\usepackage{lmodern,microtype,csquotes}
\usepackage{tikz, tikz-cd}
\usetikzlibrary{decorations.markings}
\usetikzlibrary{shapes.geometric}
\usepackage{comment, stackrel}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{leftidx}

\theoremstyle{plain}
\newtheorem{Theorem}{Theorem}[section]
\newtheorem{Fact}[Theorem]{Fact}
\newtheorem{Claim}[Theorem]{Claim}
\newtheorem{Proposition}[Theorem]{Proposition}
\newtheorem{Corollary}[Theorem]{Corollary}
\newtheorem{Lemma}[Theorem]{Lemma}
\newtheorem{Conjecture}[Theorem]{Conjecture}

\theoremstyle{definition}
\newtheorem{Definition}[Theorem]{Definition}
\newtheorem{Assumption}[Theorem]{Assumption}
\newtheorem*{Notation}{Notation}

\theoremstyle{remark}
\newtheorem{Remark}[Theorem]{Remark}
\newtheorem{Remarks}[Theorem]{Remarks}
\newtheorem{Example}[Theorem]{Example}
\newtheorem{Examples}[Theorem]{Examples}
\newtheorem{Warning}[Theorem]{Warning}

\input{macros}

\newcommand\NN{{\mathbb{N}}}
\newcommand\ZZ{{\mathbb{Z}}}
\newcommand\CC{{\mathbb{C}}}

\newcommand\as[1]{\mathbb{A}^{#1}}
\newcommand\ps[1]{\mathbb{P}^{#1}}

\newcommand\Ext{\operatorname{Ext}}
\newcommand\1{\mathbf{1}}
\newcommand\tr{\operatorname{tr}}
\newcommand\DD{\mathbb{D}}
\newcommand\isoto{\xrightarrow{\sim}}

\newcommand\catDPerf[1]{\mathrm{D}^{\mathrm{perf}}(#1)}
\newcommand\sO{{\sheaf O}}
\newcommand\sheafHom{\underline{\Hom}}

\DeclareMathOperator\Sym{Sym}
\newcommand\SL{\mathrm{SL}}

\mathchardef\mhyphen="2D
\newcommand{\da}[1]{\bigg\downarrow\rlap{$\scriptstyle{#1}$}}
\newcommand{\ra}[1]{\kern-1.5ex\xrightarrow{\ \ #1\ \ }\phantom{}\kern-1.5ex}
\newcommand{\la}[1]{\kern-1.5ex\xleftarrow{\ \ #1\ \ }\phantom{}\kern-1.5ex}


\newcommand\adam{\color{red} \'Ad\'am:\ }
\newcommand\clemens{\color{green!80!black} Clemens:\ }
\definecolor{purple}{rgb}{0.5, 0.0, 0.5}
\newcommand\timothy{\color{purple} Timothy:\ }
\usepackage{mparhack}

\sloppy

\author{Ádám Gyenge}
\author{Clemens Koppensteiner}
\address{Mathematical Institute, University of Oxford, Andrew Wiles Building, Radcliffe Observatory Quarter, Woodstock Road, OX2 6GG, Oxford UK}
\email{Adam.Gyenge@maths.ox.ac.uk}
\email{Clemens.Koppensteiner@maths.ox.ac.uk}

\author{Timothy Logvinenko}
\address{School of Mathematics, Cardiff University, Senghennydd Road, CF24 4AG, Cardiff UK}
\email{LogvinenkoT@cardiff.ac.uk}

\begin{document}



Recall from Section~\ref{subsec:prelim_grothendieck_group} the definition of the numerical Grothendieck group $\numGgp{}$ of a dg category. 
With this we form the Grothendieck group $K_0(\hcat\basecat)$ as the $\kk$-linear category with the same objects as $\hcat\basecat$ and morphism spaces
\[
    \Hom_{K_0(\hcat\basecat)}(m,n) = \numGgp{\Hom_{\hcat\basecat}(m,n)}.
\]

\begin{Assumption}\label{as:serre_homs}
    For the remainder of this subsection we assume that $\basecat$ is endowed with a bounded t-structure such that for any $A, B \in \basecat^\heartsuit$ one has $H^i\Hom_{\basecat}(A, B) = 0$ for all $i < 0$ and $H^i\Hom_{\basecat}(S^nA, B) = 0$ for all $n > 0$ and $i \le 0$.
\end{Assumption}

\begin{Lemma}
    $K_0(\hcat\basecat)$ is generated as a $k$-algebra by the classes of $\PP_A^{(n)}$ and $\QQ_A^{(n)}$ for $n \ge 1$ and $A \in \basecat$.
\end{Lemma}

\begin{proof}
    Let $X$ be any 1-morphism in $\hcat\basecat$.
    Then $X$ is a homotopy idempotent of a twisted complex built out of shifts of compositions of $\PP_A$, $\QQ_A$ and $\RR_A$ for $A \in \basecat$.
    Since we are only interested in the class of $X$ in $K^0(\basecat\hcat)$, we are only need to understand $X$ up to homotopy equivalence.

    Since the star 2-morphism from $\PP_{SA}$ to $\RR_A$ is a homotopy equivalence and the Serre functor is essentially surjective, $X$ is homotopy equivalent [?] to a homotopy idempotent of a twisted complex constructed out of shifts of $\PP_A$ and $\QQ_A$.
    By the homotopy Heisenberg relations we can further assume that $X$ is a homotopy idempotent of a twisted complexes constructed out of shifts of 1-morphisms of the form 
    \[
        \prod_{A \in \basecat} \PP_A^{m_A} \prod_{A \in \basecat} \QQ_A^{n_A}
    \]
    with only finitely many $m_A$, $n_A$ non-zero.
    Since $\basecat$ is endowed with a bounded t-structure, we can further assume that the above products only run over $A \in \basecat^\heartsuit$.
    Write $S$ for the set of all such $1$-morphisms with source and target the same as those of $X$.

    We will now follow the proof of \cite[Theorem~4.28]{ghw}.
    Thus $X$ is determined by $(Y,e)$ where $Y$ is a twisted complex constructed out of objects in $S$ and $e$ is a homotopy idempotent of $Y$.
    Without loss of generality, we can assume that $Y$ is a one-sided twist of 
    \[
        \bigoplus_{0 < i < 2N-1} \Sigma^{-i} Y_i, \qquad Y_i \in S
    \]
    for some sufficiently large $N$.
    Here we write $\Sigma$ for the shift/suspension functor lowering cohomological degree.
    In particular, we assume that all non-zero $Y_i$ are in positive degree

    By \cite[Section~4.4]{ghw}, we can lift $e$ to an $A_\infty$-idempotent $(Y, \underline e, \underline h)$.
    Following Remark~4.14 and Lemma~4.12 of \cite{ghw}, one builds a twisted complex $Z$ out of $2N$ copies of $Y$ which is homotopy equivalent to the image of $e$ acting on $Y \oplus \Sigma^{2N-1}Y$.
    We note that $\Sigma^{2N-1}Y$ is a twist of
    \[
        \bigoplus_{0 < i < 2N-1} \Sigma^{2N-1-i} Y_i, \qquad Y_i \in S.
    \]
    In particular, it is in negative degrees.

    Write $Z$ as a twist of $\bigoplus \Sigma^iZ_i$ with $Z_i \in S$ and decompose $Z$ into $Z_{<0}$, $Z_0$, $Z_{>0}$ corresponding to summands in negative, zero, and positive degrees respectively.
    By the Lemma below we obtain a diagram
    \[
        \begin{tikzcd}
            \Sigma^{2N-1} Y \arrow[d] \arrow[dr] \arrow[drr] & & Y \arrow[d] \\
            Z_{<0} \arrow[d] \arrow[r] \arrow[rr,bend left] & Z_0 \arrow[r] \arrow[dr] & Z_{>0} \arrow[d] \\
            \Sigma^{2N-1} Y & & Y
        \end{tikzcd}
    \]
    where the top half represents the projection $\pi\colon Y \oplus \Sigma^{2N-1}Y \to Z$ and the bottom half represents the inclusion $\iota\colon Z \to Y \oplus \Sigma^{2N-1}Y$.
    [I am confused about the map in the middle row, since these are presumably degree $1$ and $2$. So this diagram is not commuting?]

    By construction, $\pi \circ \iota$ is homotopic to the identity on $Z$, and from the diagram we deduce that it vanishes on $Z_0$.
    Therefor $\Id_{Z_0}$ is null-homotopic.
    [Some argument with chain complexes?]
    Thus we have a homotopy $h \in \End(Z)$ such that ${dh + hd}|_{Z_0} = \Id_{Z_0}$.
    It follows that
    \[
        (dh)^2 = (dh + hd)dh = dh,
        \quad
        (hd)^2 = hd(dh + hd) = hd,
    \]
    so that $dh$ and $hd$ are two orthogonal idempotents on $Z_0 \in S$.
    By Lemma~\ref{lem:heart-idempotents} we obtain a corresponding splitting $Z_0 = Q \oplus Q'$ with $Q$ and $Q'$ generated by $\PP_A^{(\lambda)}$ and $\QQ_A^{(\lambda}$ for $A \in \basecat^{\heartsuit}$ and $\lambda$ some partitions.
    
    Let $T$ be the cone of the composition $\Sigma^{-1}Q \to \Sigma^{-1} Z_0 \to Z_{>0}$, a twisted complex built out of $Q$ and $Z_{>0}$.
    T is a subcomplex of $Z$ and we obtain induced maps $\iota' \colon T \to Y$ and $\pi'\colon Y \to T$ as restrictions of $\iota$ and $\pi$.
    Then $i' \circ \pi' = (i \circ \pi)|_{Y} \cong e$ and [some argument with complexes?] $\pi' \circ i' \cong \id_T$.
    Therefore $T$ represents the image of $e$ and hence is homotopy equivalent to $X$.

    It follows that the class of $X$ can be expressed in terms of the classes of $\PP_A^{(\lambda)}$ and $\QQ_A^{(\lambda)}$ for $A \in \basecat^\heartsuit$ and $\lambda$ some partitions.
    From this a standard argument shows that the classes of $\PP_A^{(n)}$ and $\QQ_A^{(n)}$  generate the $K_0(\hcat\basecat)$.
\end{proof}

\begin{Lemma}
    There are no negative degree 2-morphisms 
    \[
        \prod_{A \in \basecat^\heartsuit} \PP_A^{m_A} \prod_{A \in \basecat^\heartsuit} \QQ_A^{n_A} 
        \to
        \prod_{A \in \basecat^\heartsuit} \PP_A^{m'_A} \prod_{A \in \basecat^\heartsuit} \QQ_A^{n'_A}. 
    \]
\end{Lemma}

\begin{Lemma}\label{lem:heart-idempotents}
    Any direct summand of a 1-morphism $\prod_{A \in \basecat^\heartsuit} \PP_A^{m_A} \prod_{A \in \basecat^\heartsuit} \QQ_A^{n_A} \in \hcat\basecat$ is of the form $\prod_{A \in \basecat^\heartsuit} \PP_A^{\lambda_A} \prod_{A \in \basecat^\heartsuit} \QQ_A^{\mu_A}$ for some partitions $\lambda_A$ and $\mu_A$.
\end{Lemma}

\bibliographystyle{amsplain}
\bibliography{2cat}

\end{document}
